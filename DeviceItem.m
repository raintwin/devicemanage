//
//  DeviceItem.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-9.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "DeviceItem.h"


@implementation DeviceItem

@dynamic barcode;
@dynamic brand;
@dynamic buyDate;
@dynamic category;
@dynamic deviceId;
@dynamic deviceName;
@dynamic deviceNo;
@dynamic deviceStatus;
@dynamic price;
@dynamic type;

@end

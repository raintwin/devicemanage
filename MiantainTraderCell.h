//
//  MiantainTraderCell.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaintainTraderItem.h"
@interface MiantainTraderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *telephoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactLabel;

- (void)populateCellWithEvent:(MaintainTraderItem *)item;
@end

//
//  Brand.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "Brand.h"


@implementation Brand

@dynamic brand_id;
@dynamic name;
@dynamic type_id;
@dynamic modify_date;

@end

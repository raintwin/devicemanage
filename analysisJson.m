//
//  analysisJson.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-8.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "analysisJson.h"

@implementation analysisJson
+ (void)analysisLoginJson:(id)JSONValue;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:[JSONValue objectForKey:@"type"] forKey:@"type"];
    [defaults setObject:[JSONValue objectForKey:@"userName"] forKey:@"userName"];
    [defaults setObject:[NSNumber numberWithInt:[[JSONValue objectForKey:@"userId"] intValue]] forKey:@"userId"];
    [defaults setObject:[NSNumber numberWithInt:[[JSONValue objectForKey:@"schoolId"]intValue]] forKey:@"schoolId"];
    [defaults setObject:[NSNumber numberWithInt:[[JSONValue objectForKey:@"repairId"] intValue]]forKey:@"repairId"];
    [defaults synchronize];

}
+ (id)analysisJsonData:(id)JSONValue;
{
    if ([JSONValue isKindOfClass:[NSNull class]]) {
        return nil;
    }
    else
        return JSONValue;
}
+ (NSString *)getCharterKey
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 1 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 3)
        return @"schoolId";
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 2)
        return @"repairId";
    return nil;
}
+ (NSNumber *)getCharterVulue
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 1 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 3)
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 2)
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"repairId"];

    return nil;
}
+ (NSString *)getCharterReq
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 1 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 3)
        return REQ_MAINTAINRECORD;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue] == 2)
        return REQ_REPAIRRECORD;

    return nil;
}
+ (int )getCharterType
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] intValue];
}
@end

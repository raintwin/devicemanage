//
//  MaintainRecordItem.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-25.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MaintainRecordItem.h"


@implementation MaintainRecordItem

@dynamic address;
@dynamic completionTime;
@dynamic content;
@dynamic deviceName;
@dynamic deviceNo;
@dynamic deviceType;
@dynamic maintenanceId;
@dynamic modifyDate;
@dynamic otherMoney;
@dynamic phone;
@dynamic repair;
@dynamic repairContent;
@dynamic repairPerson;
@dynamic status;
@dynamic time;
@dynamic userName;
@dynamic appraisal;
@dynamic manflag;
@dynamic acceptFlag;

@end

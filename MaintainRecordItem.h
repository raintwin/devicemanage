//
//  MaintainRecordItem.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-25.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MaintainRecordItem : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * completionTime;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * deviceNo;
@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSNumber * maintenanceId;
@property (nonatomic, retain) NSString * modifyDate;
@property (nonatomic, retain) NSString * otherMoney;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * repair;
@property (nonatomic, retain) NSString * repairContent;
@property (nonatomic, retain) NSString * repairPerson;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * time;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * appraisal;
@property (nonatomic, retain) NSString * manflag;
@property (nonatomic, retain) NSString * acceptFlag;

@end

//
//  MaintainViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-9.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MaintainViewController.h"

@interface MaintainViewController () <MaintaintraderViewCotrollerDelegate>

@property (nonatomic, retain)NSMutableDictionary *requestDict;
@end

@implementation MaintainViewController

@synthesize deviceInof = _deviceInof;
@synthesize maintainTrader = _maintainTrader;
@synthesize userNameTextField;
@synthesize deviceNoTextField;
@synthesize repairTextField;
@synthesize locationTextField;
@synthesize contanTextField;
@synthesize repairButton;
@synthesize deviceId;
@synthesize requestDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.rootDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.requestDict = [[NSMutableDictionary alloc]init];
    
    UIImage *imageBtn = [UIImage imageNamed:@"提交申请_03"];
    UIImage *backgroundButtonImage = [UIImage imageNamed:@"提交申请_03"];
    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
    
    UIButton *subitButton = [[UIButton alloc] initWithFrame:buttonRect];
    [subitButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [subitButton setTitle:@"提交申请" forState:UIControlStateNormal];
    subitButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    subitButton.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
    [subitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subitButton addTarget:self action:@selector(submitMaintainDevice) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *subitItem = [[UIBarButtonItem alloc] initWithCustomView:subitButton];
    self.navigationItem.rightBarButtonItem = subitItem;
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self queryDeviceInfo];
    self.userNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    
    self.locationTextField.delegate = self;
    self.repairTextField.delegate = self;
    self.contanTextField.delegate = self;
}
#pragma mark 提交借出申请
-(void)submitMaintainDevice
{
    [self textFiledReturnEditing];
    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] forKey:@"userName"];
    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];

    [self.requestDict setObject:REQ_APPLYREPAIR forKey:KEY_DREQTYPE];
    [self.requestDict setObject:self.locationTextField.text forKey:@"location"];
    [self.requestDict setObject:self.contanTextField.text forKey:@"contant"];
    

    [self.rootDelegate.viewController generalRequest:self.requestDict tag:REQUESTED_APPLYREPAIR_DODE];
    [[NSUserDefaults standardUserDefaults] setObject:self.deviceInof.deviceNo forKey:DEFDEVICENO];

}

- (IBAction)pushRepairView:(id)sender;
{
    maintaintraderViewCotroller *viewcontroller = [[maintaintraderViewCotroller alloc]initWithNibName:@"maintaintraderViewCotroller" bundle:nil];
    viewcontroller.title = @"维修商";
    viewcontroller.Delegate = self;
    viewcontroller.isDidSelect = YES;
    [self.navigationController pushViewController:viewcontroller animated:YES];

}
- (void) maintainTraderInfo:(MaintainTraderItem *)maintainTraderItem;
{
    self.repairTextField.text = maintainTraderItem.name;
    [self.requestDict setObject:maintainTraderItem.repairId forKey:@"repairId"];
}
#pragma mark --
- (void)queryDeviceInfo {
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deviceId"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceId == %@",self.deviceId];
    [request setPredicate:predicate];
    [request setFetchBatchSize:20];
    
    NSError *error = nil;
    //执行获取数据请求，返回数组
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
        return;
    }
    if ([mutableFetchResult count] > 0) {
        self.deviceInof= [mutableFetchResult objectAtIndex:0];
        self.deviceNoTextField.text = self.deviceInof.deviceNo;
        [self.requestDict setObject:self.deviceInof.deviceId forKey:@"deviceId"];
        [self.requestDict setObject:self.deviceInof.deviceNo forKey:@"deviceNo"];


    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂时该编号设备,请重新输入" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
#pragma mark
#pragma mark  ----- 
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.contanTextField) {
        [self moveView:YES];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self moveView:NO];
    return YES;
}
-(void)moveView:(BOOL)isPosition
{
    CGRect viewRect = [self.view bounds];
    if (isPosition) {
        viewRect.origin.y = -100;
    }
    else{
        viewRect.origin.x = 0;
    }
    [UIView beginAnimations:nil context:nil];
    [self.view setFrame:viewRect];
    [UIView setAnimationDuration:0.3f];
    [UIView commitAnimations];
}
- (void)textFiledReturnEditing
{
    [self.locationTextField resignFirstResponder];
    [self.contanTextField resignFirstResponder];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(0, 0 , CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    }];
}
#pragma mark

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

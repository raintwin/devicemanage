//
//  MiantainTraderCell.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MiantainTraderCell.h"

@implementation MiantainTraderCell
@synthesize nameLabel;
@synthesize telephoneLabel;
@synthesize contactLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    return self;
}
- (void)populateCellWithEvent:(MaintainTraderItem *)item;
{
    self.nameLabel.text = item.name;
    self.telephoneLabel.text  = item.telephone;
    self.contactLabel.text =item.contact;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

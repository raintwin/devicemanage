//
//  Material.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "Material.h"


@implementation Material

@dynamic material_id;
@dynamic brand_id;
@dynamic material_no;
@dynamic price;
@dynamic modify_date;

@end

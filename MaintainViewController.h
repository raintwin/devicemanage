//
//  MaintainViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-9.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "DeviceItem.h"
#import "maintaintraderViewCotroller.h"

@interface MaintainViewController : ViewController <UITextFieldDelegate>
@property (nonatomic ,retain) NSNumber *deviceId;

@property (nonatomic, strong) DeviceItem *deviceInof;
@property (nonatomic, strong) MaintainTraderItem *maintainTrader;


@property (nonatomic, weak) IBOutlet UITextField *userNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceNoTextField;
@property (nonatomic, weak) IBOutlet UITextField *repairTextField;
@property (nonatomic, weak) IBOutlet UITextField *locationTextField;
@property (nonatomic, weak) IBOutlet UITextField *contanTextField;

@property (nonatomic, weak) IBOutlet UIButton *repairButton;


- (IBAction)pushRepairView:(id)sender;

@end

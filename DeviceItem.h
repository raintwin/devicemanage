//
//  DeviceItem.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-9.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DeviceItem : NSManagedObject

@property (nonatomic, retain) NSString * barcode;
@property (nonatomic, retain) NSString * brand;
@property (nonatomic, retain) NSString * buyDate;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * deviceId;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * deviceNo;
@property (nonatomic, retain) NSString * deviceStatus;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * type;

@end

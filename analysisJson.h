//
//  analysisJson.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-8.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface analysisJson : NSObject
+ (void)analysisLoginJson:(id)JSONValue;

+ (id)analysisJsonData:(id)JSONValue;
+ (NSString *)getCharterKey;
+ (NSNumber *)getCharterVulue;
+ (NSString *)getCharterReq;
+ (int )getCharterType;

@end

//
//  EquipView.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-4.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//
    
#import "EquipView.h"

@implementation EquipView
@synthesize equipNameLabel,equipIdLabel;
//@synthesize equipNameField,equipIdField;
@synthesize maintainButton; 
@synthesize revertButton;
@synthesize loanButton; 
@synthesize maintainingButton;
@synthesize badButton;
@synthesize popupCellButton;
@synthesize didReverButton;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UINib *nib = nil;
        nib = [UINib nibWithNibName:@"EquipView" bundle:nil];
        NSArray *objects = [nib instantiateWithOwner:nil options:nil];
        self = [objects objectAtIndex:0];
    }
    return self;
}
- (void)awakeFromNib
{
    self.loanButton.hidden = YES;
    self.maintainButton.hidden = YES;
    self.maintainingButton.hidden = YES;
    self.revertButton.hidden = YES;
    self.badButton.hidden = YES;
    self.didReverButton.hidden = YES;

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  MEConfig.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-1.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#ifndef MaintenanceEquipment_MEConfig_h
#define MaintenanceEquipment_MEConfig_h
#import "UIFont+FSPExtensions.h"
#import "UIColor+FSPExtensions.h"


typedef enum _RequestCode {
    
    REQUESTED_LOGIN_CODE = 0,   //------------- 借出
    REQUESTED_EQUIPITEM_CODE , // ------------- 设备列表
    REQUESTED_SUBMITLOAN_CODE , // ------------- 提交设备借出

    REQUESTED_LOANITEM_CODE,  // -------------借出列表
    REQUESTED_REVERTDEVICE_DODE, // -------------归还设备
    REQUESTED_APPLYREPAIR_DODE, // ------------- 提交设备维修
    REQUESTED_MAINTAINTRADER_CODE, // ------------- 维修商
    REQUESTED_MAINTAINTCANCEL_CODE, // -------------撤销 
    REQUESTED_MAINTAINRECORD_CODE, // ------------- 设备维修列表
    REQUESTED_REPAIRER_CODE, //------------- 维修员列表
    REQUESTED_DELEGATEREPAIRER,// ------------- 委托 维修员
    REQUESTED_GETREPAIRER, // ------------- 获取 对应的维修员
    REQUESTED_GETDICTIONARY_CODE, // ------------- 获取 配件字典
    REQUESTED_CORRESPODPARTS_CODE, // ------------查询对应的配件
    REQUESTED_SUBMITREPAIRINFO_CODE, //提交维修信息
    REQUESTED_RECEVICE_CODE, //------------签收
    REQUESTED_REPAIRRECORD_CODE 
}RequestCode;


typedef enum _character {
    CTSCHOOLTYPE = 1,
    CTREAPAIRTYPE,
    CTSCHOOLAPPLYTYPE
}CharacterType;


// --------------------- 刷新数据
#define DEFDEVICENO @"DefaultDeviceNO"


// ---------------------
// ---------------------

#define WERVERURL       @"www.best1101.net"

//#define WERVERURL   @"http://www.best1101.net/MoblieAction_DMInterface"
//#define WERVERURL   @"http://192.168.1.100/MoblieAction_DMInterface"

#define DAFUSENAME  @"dafusername"
#define DAFPASSWORD @"dafpassword"

#define DAFPOST        @"dafpost"
#define DAFADDRESS     @"dafaddress"

#define MOBILETYPE  @"1"

#define HeadHeight 66.0f


// ---------------------
// ---------------------
// ---------------------
#define REQ_LOGINCODE           @"2101"
#define REQ_EQUIPCODE           @"2201"
#define REQ_LOANDDEVICE         @"2202"
#define REQ_REVERT              @"2203"
#define REQ_LOANITEM            @"2204"

#define REQ_MAINTAINTRADER      @"2301"

#define REQ_DICTIONARY          @"2401"
#define REQ_APPLYREPAIR         @"2402"
#define REQ_MAINTAINRECORD      @"2403"
#define REQ_RECEIVE             @"2404"
#define REQ_MAINTAINCANCELECORD @"2405"
//#define REQ_REQ_DELEGATEDREPAIR @"2406"
#define REQ_GETREPAIRER         @"2406"

#define REQ_REPAIRER            @"2501"
#define REQ_REPAIRRECORD        @"2502"
#define REQ_DELEGATEREPAIR      @"2503"
#define REQ_CORRESPOND          @"2504"
#define REQ_SUBMITREPAIRINFO    @"2505"

#define KEY_DRESULTCODE @"resultCode"
#define KEY_DMSG        @"msg"
#define KEY_DREQTYPE    @"reqType"
#define KEY_PAGE        @"page"
#define KEY_EVERYPAGE   @"everyPage"



#define STRING_DELEGATE @"delegate"
#endif

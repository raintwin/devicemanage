//
//  ViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-3.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "rootAppDelegate.h"
#import "QueryViewController.h"


#define TipTextPull      @"下拉可以刷新..."
#define TipTextRelease   @"松开即可刷新..."
#define TipTextLoading   @"加载中..."

#define TipViewHeight 45
#define TipViewWidth  320

@class rootViewController;
@class rootAppDelegate;
@interface ViewController : UIViewController

{
    BOOL isDropMenu;
}
@property (strong,nonatomic) rootAppDelegate *rootDelegate;
- (void) setDeviceItemRightBarButtonItems;
- (void) setDeviceItemRightBarOfquery;
- (void) setNavgationBack;
- (void) setTipViewForTabelView:(UITableView *)tabelView;
- (void)startLoading;

@property(nonatomic,retain) QueryViewController *queryViewController;
@property(nonatomic,retain) NSMutableDictionary *queryDict;

@end

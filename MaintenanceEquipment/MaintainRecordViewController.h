//
//  MaintainRecordViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-15.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LoanItemViewController.h"
#import "ViewController.h"
#import "EquipView.h"
#import "MaintainRecordCell.h"
#import "RepairerItemViewController.h"
@interface MaintainRecordViewController : ViewController <NSFetchedResultsControllerDelegate,QueryViewControllerDelegate>


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;
@end

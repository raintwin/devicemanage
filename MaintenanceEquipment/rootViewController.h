//
//  rootViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-6-27.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "rootAppDelegate.h"
#import "DeviceViewController.h"

#import "DeviceItem.h"
#import "MaintainTraderItem.h"
#import "LoanDeviceItem.h"
#import "MaintainRecordItem.h"
#import "Repairer.h"
#import "Category.h"
#import "DelegatedRapairer.h"
#import "MaintainViewController.h"
#import "LoanDeviceViewController.h"
#import "LoanItemViewController.h"
#import "MaintainRecordViewController.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "MainRecordInofViewController.h"

@class MainRecordInofViewController;

@interface rootViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>


{
}
@property (nonatomic ,assign) int characterId;
@property (nonatomic,retain)    MainRecordInofViewController *mainRecordInofViewController;

@property (strong,nonatomic) rootAppDelegate *myDelegate;

@property (weak,nonatomic) IBOutlet UITextField *useNameTextField;
@property (weak,nonatomic) IBOutlet UITextField *passWordTextField;

@property (weak,nonatomic) IBOutlet UITextField *urlTextField;


@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *loginAgainButton;


- (void)query;

- (IBAction)login:(id)sender;
- (IBAction)loginAgain:(id)sender;

- (void)requestEquipItem;
- (void)requestLoanItem;
- (void)requestMaintainRecord;
- (void)requestMaintainTrader;
- (void)requestRepairerItem;

- (void)generalRequest:(NSMutableDictionary *)postValue tag:(RequestCode)flag;
- (void)generalRequest:(NSMutableDictionary *)postValue tag:(RequestCode)flag delegate:(MainRecordInofViewController *)mainDelegate;

@end

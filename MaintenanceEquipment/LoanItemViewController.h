//
//  LoanItemViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "rootViewController.h"

@interface LoanItemViewController : ViewController<NSFetchedResultsControllerDelegate,UIAlertViewDelegate,QueryViewControllerDelegate>
{
    NSInteger didSectionFlag;
    NSInteger oldSectionFlag;
    
    BOOL isOpenCell;
    
    
    NSInteger endSection;
    NSInteger didSection;
    BOOL ifOpen;

}
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;

@end

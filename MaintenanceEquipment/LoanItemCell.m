//
//  LoanItemCell.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-12.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "LoanItemCell.h"

@implementation LoanItemCell
@synthesize loanerLabel;
@synthesize departLabel;
@synthesize loadDateLabel;
@synthesize retureDateLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.loanerLabel.text = @"";
    self.departLabel.text = @"";
    self.loadDateLabel.text = @"";
    self.retureDateLabel.text = @"";
}
- (void)populateCellWithEvent:(LoanDeviceItem *)item;
{
    [self setAccessoryType:UITableViewCellAccessoryNone];

    self.loanerLabel.text = item.lendUser;
    self.departLabel.text = item.deptName;
    self.loadDateLabel.text = item.lendDate;
    self.retureDateLabel.text = item.returnDate;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  rootAppDelegate.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-6-27.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "rootAppDelegate.h"

#import "rootViewController.h"

@implementation rootAppDelegate
@synthesize managedObjectContext       = _managedObjectContext;
@synthesize managedObjectModel         = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert];

    [[NSUserDefaults standardUserDefaults] setObject:@"fefer" forKey:@"deviceToken"];
//    [[NSUserDefaults standardUserDefaults] setObject:WERVERURL forKey:DAFADDRESS];
    [[UIApplication sharedApplication]setStatusBarStyle:NO animated:NO];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[rootViewController alloc] initWithNibName:@"rootViewController" bundle:nil];
//    self.navController = [[UINavigationController alloc]initWithRootViewController:self.viewController];
//    self.viewController.title = @"资产设备管理";
    self.window.rootViewController = self.viewController;
//    self.viewController.view.frame = [[UIScreen mainScreen] bounds];
//    [self.window addSubview:self.viewController.view];
//    self.navController.navigationBarHidden = YES;
//    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
    
    NSString *docs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSLog(@"path:%@",docs);
    

    [[NSUserDefaults standardUserDefaults]synchronize];
    return YES;
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0)
{
    NSMutableString* strDeviceToken = [[NSMutableString alloc] initWithFormat:@"%@", deviceToken];
    
    [strDeviceToken replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, strDeviceToken.length)];
    [strDeviceToken replaceOccurrencesOfString:@"<" withString:@"" options:0 range:NSMakeRange(0, strDeviceToken.length)];
    [strDeviceToken replaceOccurrencesOfString:@">" withString:@"" options:0 range:NSMakeRange(0, strDeviceToken.length)];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:strDeviceToken forKey:@"deviceToken"];
	NSLog(@"didRegister start:%@", strDeviceToken);
    
    
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:@"devicemanage"]) {
        return YES;
    }
    return YES;
    
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    application.applicationIconBadgeNumber = 0;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo NS_AVAILABLE_IOS(3_0);

{
    NSLog(@"userInfo :%@",userInfo);
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

#pragma mark -
#pragma mark - Core Data Stack

-(NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

-(NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    //得到数据库的路径
    NSString *docs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    //CoreData是建立在SQLite之上的，数据库名称需与Xcdatamodel文件同名
    NSURL *storeUrl = [NSURL fileURLWithPath:[docs stringByAppendingPathComponent:@"Equipment.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    
    return _persistentStoreCoordinator;
}

-(NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator =[self persistentStoreCoordinator];
    
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc]init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}
#pragma mark -


//这个方法定义的是当应用程序退到后台时将执行的方法，按下home键执行（通知中心来调度）
//实现此方法的目的是将托管对象上下文存储到数据存储区，防止程序退出时有未保存的数据
- (void)applicationWillTerminate:(UIApplication *)application
{
    NSError *error;
    if (self.managedObjectContext != nil) {
        //hasChanges方法是检查是否有未保存的上下文更改，如果有，则执行save方法保存上下文
        if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error]) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
            abort();
        }
    }
}

@end

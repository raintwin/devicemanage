//
//  MountingsCell.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-22.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MountingsCell.h"
#import "Category.h"
@implementation MountingsCell
@synthesize totalPriceLabel;
@synthesize deviceNameLabel;
@synthesize numberPriceLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.totalPriceLabel.text = @"";
    self.deviceNameLabel.text = @"";
    self.numberPriceLabel.text = @"";
    
    self.deviceNameLabel.numberOfLines = 2;
}
- (void)populateCellWithEvent:(NSMutableDictionary *)dicionary;
{
    self.deviceNameLabel.text = [dicionary objectForKey:@"name"];
    float price = [[dicionary objectForKey:@"price"] floatValue];
    int amount = [[dicionary objectForKey:@"amount"] intValue];
    float total = price*amount;
    if (price == 0) {
        self.numberPriceLabel.text = @"0";
        return;
    }
    self.numberPriceLabel.text = [NSString stringWithFormat:@"%d * %0.2f",amount,price];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%0.2f",total];
    
}
@end

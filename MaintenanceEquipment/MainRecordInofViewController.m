//
//  MainRecordInofViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-15.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MainRecordInofViewController.h"
#import "MountingsViewController.h"
#import "MountingsCell.h"
#import "ReceiveViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MainRecordInofViewController () <MountingsViewControllerDelegate,StatusTableControllerDelegate>
@property (nonatomic,retain)NSMutableArray *parts;
@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,retain) NSMutableDictionary *requestDict;
@end


@implementation MainRecordInofViewController


@synthesize fetchedResultsController = _fetchedResultsController;

@synthesize parts;

@synthesize maintenanceIdLabel;
@synthesize repairPersonLabel;
@synthesize signLabel;
@synthesize comTimeLabel;

@synthesize deviceTypeLabel;
@synthesize userNameLabel;
@synthesize addressLabel;
@synthesize repairLabel;
@synthesize contentLabel;

@synthesize repairContentTextView;
@synthesize maintainRecordItem;
@synthesize scorllView;
@synthesize addAccessories;
@synthesize otherTextField;
@synthesize tableView;

@synthesize optionButton;
@synthesize receiveButton;

@synthesize requestDict;

typedef enum _DeviceStatus {
    
    STATUS_APPLYING = 0,
    STATUS_MAINTAINING ,
    STATUS_WAITING,
    STATUS_COMPLETE,
    STATUS_CANCEL,
}DeviceStatus;

static NSString *const MaintainRecordCellIdentifier = @"MaintainRecordCellellIdentifier";
static NSString *const MountingsCellIdentifier = @"MountingsCellIdentifier";

static NSString *const date = @"1990-12-12 20:20:20";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.parts = [[NSMutableArray alloc]init];
    self.requestDict = [[NSMutableDictionary alloc]init];
    [self.requestDict setObject:@"1" forKey:@"type"];
    [self.requestDict setObject:maintainRecordItem.maintenanceId forKey:@"maintenanceId"];
    [self.requestDict setObject:[analysisJson getCharterVulue] forKey:@"repairId"];
    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];


    self.otherTextField.delegate = self;
    self.repairContentTextView.delegate = self;
    self.repairContentTextView.delegate = self;
    self.repairContentTextView.layer.borderColor = [UIColor grayColor].CGColor;
    self.repairContentTextView.layer.borderWidth =1.0;
    self.repairContentTextView.layer.cornerRadius =5.0;
    self.addAccessories.hidden = YES;
    self.receiveButton.hidden = YES;
    NSLog(@"maintainRecordItem.status :%@",[maintainRecordItem.status stringValue]);
    
    switch ([maintainRecordItem.status intValue]) {
        case STATUS_MAINTAINING:
            [self showEditButton];
            break;
        case STATUS_WAITING:
            if ([analysisJson getCharterType] == CTSCHOOLTYPE || [analysisJson getCharterType] == CTSCHOOLAPPLYTYPE) {
                self.receiveButton.hidden = NO;
            }else if ([analysisJson getCharterType] == CTSCHOOLTYPE)
            {
                
            }
            self.repairContentTextView.editable = NO;
            break;
        default:
            break;
    }
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 338, 320, 80)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.scorllView addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"MountingsCell" bundle:nil] forCellReuseIdentifier:MountingsCellIdentifier];

    [self setDeviceStatus:self.maintainRecordItem.status];

    [self getRepairer];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

    
}
-(void) showEditButton
{
    if (![[analysisJson getCharterKey] isEqualToString:@"repairId"]) {
        return;
    }
    UIImage *imageBtn = [UIImage imageNamed:@"3355_05"];
    UIImage *backgroundButtonImage = [UIImage imageNamed:@"3355_05"];
    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
    
    UIButton *subitButton = [[UIButton alloc] initWithFrame:buttonRect];
    [subitButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    subitButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    subitButton.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
    [subitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subitButton setTitle:@"提交" forState:UIControlStateNormal];
    [subitButton addTarget:self action:@selector(editingMaintainInfo) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *subitItem = [[UIBarButtonItem alloc] initWithCustomView:subitButton];
    self.navigationItem.rightBarButtonItem = subitItem;
    self.addAccessories.hidden = NO;

}
#pragma mark 签收
- (IBAction)receiveButton:(id)sender
{
    ReceiveViewController *receiveViewController = [[ReceiveViewController alloc]initWithStyle:UITableViewStyleGrouped];
    receiveViewController.maintainRecordItem = self.maintainRecordItem;
    [self.navigationController pushViewController:receiveViewController animated:YES];
    
}
#pragma mark 添加配件
- (IBAction)addAccessories:(id)sender
{
    MountingsViewController *mountingsViewController = [[MountingsViewController alloc]initWithNibName:@"MountingsViewController" bundle:nil];
    mountingsViewController.delegate = self;
    [self.navigationController pushViewController:mountingsViewController animated:YES];
}
-(void)setPartCategory:(Category *)category amount:(int)amount;
{
    NSLog(@"category.material_no :%@",category.material_no);
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:category.material_id forKey:@"materialId"];
    [dict setObject:[NSNumber numberWithInt:amount] forKey:@"amount"];
    [dict setObject:category.material_no forKey:@"name"];
    if ([category.price intValue] > 0) {
        [dict setObject:category.price forKey:@"price"];
    }
    else
    {
        [dict setObject:[NSNumber numberWithInt:0] forKey:@"price"];
    }
    [self.parts addObject:dict];
    [self.tableView reloadData];
    NSLog(@"self.parts :%@",self.parts);
}
#pragma mark 选择状态
- (IBAction)setStauts:(id)sender
{
//    [self.optionButton setTitle:@"fefe" forState:UIControlStateNormal];
    StatusTableController *statusTableController = [[StatusTableController alloc]initWithStyle:UITableViewStyleGrouped];
    statusTableController.delegate = self;
    [self.navigationController pushViewController:statusTableController animated:YES];
}
-(void)setStatusIndex:(NSString *)index;
{
    NSLog(@"statu=%@",index);
    if ([index isEqualToString:@"1"]) {
        [self.optionButton setTitle:@"正在维修" forState:0];

    }else if ([index isEqualToString:@"2"])
    {
        [self.optionButton setTitle:@"完成" forState:0];

    }

    [self.requestDict setObject:index forKey:@"type"];

}
#pragma mark
#pragma mark ---- 提交维修情况
- (void) editingMaintainInfo
{
    if ([self.otherTextField.text length]>0) {
        [self.requestDict setObject:[NSNumber numberWithDouble:[self.otherTextField.text doubleValue]] forKey:@"otherMoney"];
    }else{
        [self.requestDict setObject:[NSNumber numberWithDouble:0.0f] forKey:@"otherMoney"];
    }
    
    if ([self.contentLabel.text length]>0) {
        [self.requestDict setObject:self.repairContentTextView.text forKey:@"content"];
    }else{
        [self.requestDict setObject:@"" forKey:@"content"];
    }
    [self.requestDict setObject:self.parts forKey:@"materials"];
    [self.requestDict setObject:REQ_SUBMITREPAIRINFO forKey:KEY_DREQTYPE];
    [self.rootDelegate.viewController generalRequest:self.requestDict tag:REQUESTED_SUBMITREPAIRINFO_CODE];
    [[NSUserDefaults standardUserDefaults] setObject:maintainRecordItem.deviceNo forKey:DEFDEVICENO];

}
#pragma mark -----
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.scorllView.frame = [self.view bounds];
    [self.scorllView setContentSize:CGSizeMake(0, 630)];
    
//    [self setDeviceStatus:self.maintainRecordItem.status];
    self.maintenanceIdLabel.text = [self.maintainRecordItem.maintenanceId stringValue];
    self.comTimeLabel.text = self.maintainRecordItem.completionTime;
    self.repairPersonLabel.text = self.maintainRecordItem.repairPerson;
    
    self.deviceTypeLabel.text = self.maintainRecordItem.deviceType;
    self.userNameLabel.text = self.maintainRecordItem.userName;
    self.addressLabel.text = self.maintainRecordItem.address;
    self.repairLabel.text = self.maintainRecordItem.repair;
    self.contentLabel.text = self.maintainRecordItem.content;
    
    self.repairContentTextView.text = self.maintainRecordItem.repairContent;
    self.otherTextField.text = self.maintainRecordItem.otherMoney;
    if ([self.maintainRecordItem.status intValue] == 3) {
        self.signLabel.text = [NSString stringWithFormat:@"%@ 技术水平:%@ 服务态度:%@",self.maintainRecordItem.appraisal,[self acceptOpinion:self.maintainRecordItem.acceptFlag],[self acceptOpinion:self.maintainRecordItem.manflag]];
    }
    
}
-(NSString *)acceptOpinion:(NSString *)flag
{
    switch ([flag intValue]) {
        case 0:
        return @"非常满意";
            break;
        case 1:
            return @"满意";
            break;
        case 2:
            return @"一般";
            break;
        case 3:
            return @"差";
            break;
        default:
            break;
    }
    return nil;
}
#pragma mark 获取对应的维修员
-(void) getRepairer
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"schoolId"];
    [dict setObject:REQ_GETREPAIRER forKey:KEY_DREQTYPE];
    [dict setObject:self.maintainRecordItem.maintenanceId forKey:@"maintenanceId"];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_GETREPAIRER delegate:self];
}
#pragma mark 获取对应的配件
-(void) getPartsItem
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"repairId"];
    [dict setObject:REQ_CORRESPOND forKey:KEY_DREQTYPE];
    [dict setObject:self.maintainRecordItem.maintenanceId forKey:@"maintenanceId"];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_CORRESPODPARTS_CODE delegate:self];
}
-(void)setPartsItem:(NSMutableArray *)item
{
//    self.parts = item;
    if ([item count] == 0) {
        return;
    }
    [self.parts removeAllObjects];
    self.parts = [NSMutableArray arrayWithArray:item];
    [self.tableView reloadData];
}
#pragma mark

-(void)setDeviceStatus:(NSNumber *)deviceId
{
    //deviceStatus true 已归还
    [self.optionButton setEnabled:NO];
    NSString *statusString = nil;
    switch ([deviceId integerValue]) {
        case STATUS_APPLYING:
            statusString = @"申请中";
            break;
        case STATUS_CANCEL:
            statusString = @"已撤销";
            break;
        case STATUS_COMPLETE:
            statusString = @"完成 已签收";
            break;
        case STATUS_MAINTAINING:
            statusString = @"维修中";
            if([analysisJson getCharterType] == CTREAPAIRTYPE) {
                [self.optionButton setEnabled:YES];
            }
            else
                [self.optionButton setEnabled:NO];
            break;
        case STATUS_WAITING:
            statusString = @"完成 未签收";
            break;
        default:
            break;
    }
//    self.statusLabel.text = statusString;
    [self.optionButton setTitle:statusString forState:UIControlStateNormal];
}

- (void)loadView
{
    [super loadView];
    

    
    self.maintenanceIdLabel.text = @"";
    self.comTimeLabel.text = @"";
    self.repairPersonLabel.text = @"";
    self.signLabel.text = @"";
    
    self.deviceTypeLabel.text = @"";
    self.userNameLabel.text = @"";
    self.addressLabel.text = @"";
    self.repairLabel.text = @"";
    self.contentLabel.text = @"";
    
    self.repairContentTextView.text = @"";
    self.addAccessories.hidden = YES;

//    [self.repairContentTextView setEditable:NO];

}
#pragma mark
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.parts count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    MaintainRecordItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSMutableDictionary *dict = [self.parts objectAtIndex:[indexPath row]];
    MountingsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MountingsCellIdentifier];
    [cell populateCellWithEvent:dict];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.maintainRecordItem.status intValue] == 3 || [self.maintainRecordItem.status intValue] == 2) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.parts removeObjectAtIndex:indexPath.row];
        // Delete the row from the data source.
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}
#pragma mark textfield


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(0, 0 , CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    }];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(0, -200 , CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    }];
}
#pragma mark
#pragma mark --
- (NSFetchedResultsController *)fetchedResultsController
{
    if (nil != _fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *playerEntity = [NSEntityDescription entityForName:@"DelegatedRapairer" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    [fetchRequest setEntity:playerEntity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"flag"ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"team == %@",@"dd"];
    //    [fetchRequest setPredicate:predicate];
    //    [fetchRequest setFetchBatchSize:20];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.rootDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:@"MaintainRecordItem"];
    _fetchedResultsController.delegate = self;
    
    NSError *error = NULL;
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _fetchedResultsController;
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.fetchedResultsController) {
        if (self.fetchedResultsController.fetchedObjects.count >0) {
            
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

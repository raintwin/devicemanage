//
//  Category.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-22.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Category : NSManagedObject

@property (nonatomic, retain) NSNumber * category_id;
@property (nonatomic, retain) NSString * modify_date;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * material_no;
@property (nonatomic, retain) NSNumber * brand_id;
@property (nonatomic, retain) NSNumber * type_id;
@property (nonatomic, retain) NSNumber * material_id;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * flag;

@end

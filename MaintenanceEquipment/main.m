//
//  main.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-6-27.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "rootAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([rootAppDelegate class]));
    }
}

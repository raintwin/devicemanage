//
//  DeviceViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-3.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "DeviceItem.h"
#import "rootViewController.h"
#import "QueryViewController.h"
@interface DeviceViewController : ViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,ZBarReaderDelegate,QueryViewControllerDelegate>
{
    NSInteger didSectionFlag;
    NSInteger oldSectionFlag;
    
    BOOL isOpenCell;
    

    
    NSInteger endSection;
    NSInteger didSection;
    BOOL ifOpen;
    
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller;

@end

//
//  QueryViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-31.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "QueryViewController.h"

#define DateViewHeight 304

@interface QueryViewController ()
@property(nonatomic,strong) UIView *dateView;
@property(nonatomic,strong) UIDatePicker *datePicker;
@property(nonatomic,strong) UIToolbar *toolBar;
@property(nonatomic,retain) NSMutableDictionary *dataDictionary;
@property(nonatomic,retain) UITextField *loanDeviceNoTextField;

@property(nonatomic,retain) UITextField *mianDevNoTextField;
@property(nonatomic,retain) UITextField *mianDevNameTextField;

@end

@implementation QueryViewController

@synthesize queryButton;
@synthesize deviceNoTextField;
@synthesize deviceNameTextField;

@synthesize delegate;
@synthesize deviceView;
@synthesize loanView;

@synthesize datePicker;
@synthesize toolBar;
@synthesize dateView;

@synthesize mianView;

@synthesize dataDictionary;
@synthesize loanDeviceNoTextField;
@synthesize mianDevNoTextField;
@synthesize mianDevNameTextField;
@synthesize loanTableView = _loanTableView;
@synthesize mainTableView = _mainTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark
-(void)loadView
{
    [super loadView];
    self.deviceView.hidden = YES;
    self.loanView.hidden = YES;
    self.mianView.hidden = YES;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    currentIndex = -1;

    self.deviceNameTextField.delegate = self;
    self.deviceNoTextField.delegate = self;
    self.deviceNameTextField.placeholder = @"可填";
    self.deviceNoTextField.placeholder = @"可填";
    
    self.loanTableView.delegate = self;
    self.loanTableView.dataSource = self;
    [self.loanTableView setScrollEnabled:NO];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.mainTableView setScrollEnabled:NO];
    
    self.dataDictionary = [[NSMutableDictionary alloc]init];
    
    [self.dataDictionary setObject:REQ_LOANITEM forKey:KEY_DREQTYPE];
    [self.dataDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [self.dataDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];;
    
    [self.dataDictionary setObject:@"" forKey:@"status"];
    [self.dataDictionary setObject:@"" forKey:@"lendDateF"];
    [self.dataDictionary setObject:@"" forKey:@"lendDateL"];
    [self.dataDictionary setObject:@"" forKey:@"deviceNo"];
    [self.dataDictionary setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [self.dataDictionary setObject:[NSNumber numberWithInt:1000] forKey:@"everyPag"];
    
}

- (IBAction)queryButton:(id)sender;
{
    [self.deviceNameTextField resignFirstResponder];
    [self.deviceNoTextField resignFirstResponder];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if ([self.deviceNoTextField.text length] > 0) {
        [dict setObject:self.deviceNoTextField.text forKey:@"deviceNo"];
    }
    if ([self.deviceNameTextField.text length] > 0 ) {
        [dict setObject:self.deviceNameTextField.text forKey:@"deviceName"];
    }
    [self.delegate queryDictiony:dict];
}

#pragma mark
#pragma mark tableview
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.loanTableView == tableView) {
        switch (section) {
            case 0:
                return @"设备编号 (可填)";
                break;
            case 1:
                return @"设备状态（可选）";
                break;
            case 2:
                return @"开始时间";
                break;
            case 3:
                return @"结束时间";
                break;
            default:
                break;
        }
    }
    if (self.mainTableView == tableView) {
        switch (section) {
            case 0:
                return @"设备编号 (可填)";
                break;
            case 1:
                return @"设备名称（可选）";
                break;
            case 2:
                return @"设备状态（可选）";
                break;
            default:
                break;
        }
    }
    return nil;
}
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.f;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.loanTableView == tableView) {
        return 5;
    }
    if (self.mainTableView == tableView) {
        return 4;
    }
    return 0;
    //    return [[self.fetchedResultsController sections] count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.loanTableView == tableView) {
        if (section == 1) {
            return 3;
        }
        return 1;
    }
    if (self.mainTableView == tableView) {
        if (section == 2) {
            return 5;
        }
        return 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PartsViewControllerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell =
        [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (self.loanTableView == tableView) {
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [tableView setSeparatorColor:[UIColor grayColor]];
        
        if ([indexPath section] == 0) {
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [tableView setSeparatorColor:[UIColor clearColor]];
            [cell setBackgroundColor:[UIColor clearColor]];
            self.loanDeviceNoTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 2, 300, 35)];
            self.loanDeviceNoTextField.borderStyle=UITextBorderStyleRoundedRect;
            self.loanDeviceNoTextField.text = [self.dataDictionary objectForKey:@"deviceNo"];
            self.loanDeviceNoTextField.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:self.loanDeviceNoTextField];
            self.loanDeviceNoTextField.delegate = self;
            self.loanDeviceNoTextField.placeholder = @"设备编号";
        }
        if ([indexPath section] == 1) {
            switch ([indexPath row]) {
                case 0:
                    cell.textLabel.text = @"全部";
                    break;
                case 1:
                    cell.textLabel.text = @"借出";
                    break;
                case 2:
                    cell.textLabel.text = @"已归还";
                    break;
                default:
                    break;
            }
        }
        if ([indexPath section] == 2) {
            cell.textLabel.text = [self.dataDictionary objectForKey:@"lendDateF"];
        }
        if ([indexPath section] == 3)  {
            cell.textLabel.text = [self.dataDictionary objectForKey:@"lendDateL"];
        }
        if ([indexPath section] == 4) {
            cell.textLabel.text = @"收起";
            cell.textLabel.textAlignment = UITextAlignmentCenter;
        }

    }
    
    if (self.mainTableView == tableView) {
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [tableView setSeparatorColor:[UIColor grayColor]];

        if ([indexPath section] == 0) {
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [tableView setSeparatorColor:[UIColor clearColor]];
            [cell setBackgroundColor:[UIColor clearColor]];
            self.mianDevNoTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 2, 300, 35)];
            self.mianDevNoTextField.borderStyle=UITextBorderStyleRoundedRect;
            self.mianDevNoTextField.text = [self.dataDictionary objectForKey:@"deviceNo"];
            self.mianDevNoTextField.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:self.mianDevNoTextField];
            self.mianDevNoTextField.delegate = self;
            self.mianDevNoTextField.placeholder = @"设备编号";
        }
        if ([indexPath section] == 1) {
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [tableView setSeparatorColor:[UIColor clearColor]];
            [cell setBackgroundColor:[UIColor clearColor]];
            self.mianDevNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 2, 300, 35)];
            self.mianDevNameTextField.borderStyle=UITextBorderStyleRoundedRect;
            self.mianDevNameTextField.text = [self.dataDictionary objectForKey:@"deviceNo"];
            self.mianDevNameTextField.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:self.mianDevNameTextField];
            self.mianDevNameTextField.delegate = self;
            self.mianDevNameTextField.placeholder = @"设备名称";
        }
        if ([indexPath section] == 2) {
            switch ([indexPath row]) {
                case 0:
                    cell.textLabel.text = @"申报中";
                    break;
                case 1:
                    cell.textLabel.text = @"维修中";
                    break;
                case 2:
                    cell.textLabel.text = @"完成 待签收";
                    break;
                case 3:
                    cell.textLabel.text = @"完成 已签收";
                    break;
                case 4:
                    cell.textLabel.text = @"已撤销";
                    break;
                default:
                    break;
            }

        }
        if ([indexPath section] == 3) {
            cell.textLabel.text = @"收起";
            cell.textLabel.textAlignment = UITextAlignmentCenter;
        }

    }
    return cell;
}
//- (void)getSystemDate
//{
//    NSDate *  senddate=[NSDate date];
//    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
//    [dateformatter setDateFormat:@"HH:mm"];
//    [dateformatter setDateFormat:@"YYYY-MM-dd"];
//    NSString *  morelocationString=[dateformatter stringFromDate:senddate];
//    [self.dataDictionary setObject:morelocationString forKey:@"dateFirst"];
//    [self.dataDictionary setObject:morelocationString forKey:@"dateEnd"];
//}

#pragma mark 日期
- (void)datePickerView:(NSInteger)flag;
{
    if (self.dateView == nil) {
        self.dateView = [[UIView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, DateViewHeight)];
        if (self.datePicker == nil) {
            self.datePicker = [[UIDatePicker alloc]init];
        }
        self.datePicker.center = self.view.center;
        self.datePicker.datePickerMode=UIDatePickerModeDate;
        self.datePicker.frame = CGRectMake(0,44 , 320, 260);
        
        //创建工具栏
        NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:3];
        UIBarButtonItem *confirmBtn = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(confirmPickView:)];
        confirmBtn.tag = flag;
        UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(pickerHide)];
        [items addObject:cancelBtn];
        [items addObject:flexibleSpaceItem];
        [items addObject:confirmBtn];
        if (self.toolBar==nil) {
            self.toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        }
        self.toolBar.hidden = NO;
        self.toolBar.barStyle = UIBarStyleBlackTranslucent;
        self.toolBar.items = items;
        [self.dateView addSubview:self.datePicker];
        [self.dateView addSubview:self.toolBar];
        [self.view addSubview:self.dateView];
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.dateView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - DateViewHeight, 320, DateViewHeight);
    }];
}
- (void)pickerHide
{
    [UIView animateWithDuration:0.5 animations:^{
        self.dateView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, DateViewHeight);
    } completion:^(BOOL finished){
        [self.dateView  removeFromSuperview];
        self.dateView = nil;
    }];

}
- (void)confirmPickView:(UIBarButtonItem *)sender
{
    NSDate* _date = self.datePicker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //    NSString *now = [dateFormatter stringFromDate:_date];
    //    [dateFormatter setDateFormat:@"HH:mm:ss"];
    //    NSString *nowTimer = [dateFormatter stringFromDate:[NSDate date]];
    //    now = [NSString stringWithFormat:@"%@ %@",now,nowTimer];
//    self.deviceDateTextField.text = [dateFormatter stringFromDate:_date];
    NSString *locationString = [dateFormatter stringFromDate:_date];
    NSLog(@"dateFormatter--:%@",[dateFormatter stringFromDate:_date]);
    if (sender.tag == 2) {
        [self.dataDictionary setObject:locationString forKey:@"lendDateF"];
    }
    if (sender.tag == 3) {
        [self.dataDictionary setObject:locationString forKey:@"lendDateL"];
    }
    [self pickerHide];
    [self.loanTableView reloadData];
}

#pragma mark

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.loanTableView == tableView) {
        if ([indexPath section] == 1) {
            if (currentIndex == indexPath.row) {
                return;
            }
            
            UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType  = UITableViewCellAccessoryCheckmark;
            if (currentIndex >= 0) {
                NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex inSection:1];
                UITableViewCell *oldCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
                oldCell.accessoryType  = UITableViewCellAccessoryNone;
            }
            currentIndex = [indexPath row];
            
        }
        if ([indexPath section] == 2 || [indexPath section] == 3) {
            [self datePickerView:[indexPath section]];
        }
        if ([indexPath section] == 4) {
            [self.delegate queryDictiony:nil];
        }
    }
    if (self.mainTableView == tableView) {
        if ([indexPath section] == 2) {
            if (currentIndex == indexPath.row) {
                return;
            }
            UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType  = UITableViewCellAccessoryCheckmark;
            if (currentIndex >= 0) {
                NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex inSection:2];
                UITableViewCell *oldCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
                oldCell.accessoryType  = UITableViewCellAccessoryNone;
            }
            currentIndex = [indexPath row];
            
        }
        if ([indexPath section] == 3) {
            [self.delegate queryDictiony:nil];
        }

    }
}

#pragma mark textfield
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.text = nil;
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark 借出列表
-(NSMutableDictionary *)getDateDictionary
{
    if (self.dataDictionary != nil) {
        if (currentIndex>0) {
            [self.dataDictionary setObject:[[NSNumber numberWithInt:currentIndex] stringValue] forKey:@"status"];
        }
        if ([self.loanDeviceNoTextField.text length] >0) {
            [self.dataDictionary setObject:self.loanDeviceNoTextField.text forKey:@"deviceNo"];
        }
        return self.dataDictionary;
    }
    return nil;
}
#pragma mark 借出维修记录
-(NSMutableDictionary *)getMianRecodeDateDictionary
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (currentIndex>=0) {
        [dict setObject:[[NSNumber numberWithInt:currentIndex] stringValue] forKey:@"deviceStatus"];
    }else
        [dict setObject:@"" forKey:@"deviceStatus"];

    if ([self.mianDevNoTextField.text length] > 0) {
        [dict setObject:self.mianDevNoTextField.text forKey:@"deviceNo"];
    }else
        [dict setObject:@"" forKey:@"deviceNo"];

    if ([self.mianDevNameTextField.text length] > 0) {
        [dict setObject:self.mianDevNameTextField.text forKey:@"deviceName"];
    }else
        [dict setObject:@"" forKey:@"deviceName"];

    return dict;
}
#pragma mark

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  MaintainRecordCell.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-15.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaintainRecordItem.h"
@interface MaintainRecordCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *deviceStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *deviceNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *applyDataLabel;

@property (weak, nonatomic) IBOutlet UIButton *OptionButton;


- (void)populateCellWithEvent:(MaintainRecordItem *)item;

@end

//
//  LoanDeviceItem.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-8-2.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "LoanDeviceItem.h"


@implementation LoanDeviceItem

@dynamic deptName;
@dynamic deviceName;
@dynamic deviceNo;
@dynamic deviceStatus;
@dynamic landOutId;
@dynamic lendDate;
@dynamic lendUser;
@dynamic returnDate;

@end

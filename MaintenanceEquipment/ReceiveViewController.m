//
//  ReceiveViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-24.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "ReceiveViewController.h"

@interface ReceiveViewController ()
@property (nonatomic,retain) NSArray *titles;
@property (nonatomic,retain)NSMutableDictionary *requestDict;
@property (nonatomic,retain) UITextField *contentTextField;

@end

@implementation ReceiveViewController
@synthesize myDelegate;
@synthesize titles;
@synthesize maintainRecordItem;
@synthesize requestDict;
@synthesize contentTextField;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.myDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return self;
}
-(void)dealloc
{
    self.requestDict = nil;
    self.titles = nil;
    self.contentTextField = nil;
}
-(void)closeButton
{
    [self.navigationController popViewControllerAnimated:YES];//
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    {
        UIImage *imageBtn = [UIImage imageNamed:@"返回"];
        UIImage *backgroundButtonImage = imageBtn;
        
        CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
        [closeButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
        [closeButton setTitle:nil forState:UIControlStateNormal];
        closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
        self.navigationItem.leftBarButtonItem = closeItem;

    }
    
    {
        UIImage *imageBtn = [UIImage imageNamed:@"3355_05"];
        UIImage *backgroundButtonImage = [UIImage imageNamed:@"3355_05"];
        CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
        
        UIButton *subitButton = [[UIButton alloc] initWithFrame:buttonRect];
        [subitButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
        subitButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [subitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [subitButton setTitle:@"签收" forState:UIControlStateNormal];
        [subitButton addTarget:self action:@selector(receiveRepair) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *subitItem = [[UIBarButtonItem alloc] initWithCustomView:subitButton];
        self.navigationItem.rightBarButtonItem = subitItem;
    }
    
    [self.tableView setScrollEnabled:NO];
    accflag = 1;
    manflag = 1;
    
    accflagCurrentIndex = 0;
    manflagCurrentIndex = 0;
    self.titles = [[NSArray alloc]initWithObjects:@"非常满意",@"满意",@"一般",@"差",nil];
    self.requestDict = [[NSMutableDictionary alloc]init];
    [self.requestDict setObject:@"" forKey:@"content"];
    [self.requestDict setObject:REQ_RECEIVE forKey:KEY_DREQTYPE];

}
-(void)receiveRepair
{
    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];

    [self.requestDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] forKey:@"userName"];
    [self.requestDict setObject:self.maintainRecordItem.maintenanceId forKey:@"maintenanceId"];
    [self.requestDict setObject:[NSNumber numberWithInt:accflagCurrentIndex+1] forKey:@"accflag"];
    [self.requestDict setObject:[NSNumber numberWithInt:manflagCurrentIndex+1] forKey:@"manflag"];
    
    if ([self.contentTextField.text length] >0 ) {
        [self.requestDict setObject:self.contentTextField.text forKey:@"content"];
    }
    [self.myDelegate.viewController generalRequest:self.requestDict tag:REQUESTED_RECEVICE_CODE];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.maintainRecordItem.deviceNo forKey:DEFDEVICENO];
}
-(void)successReceiveRepair
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[MaintainRecordViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 0 || [indexPath section] == 1) {
        return 26.f;
    }
    return 80.0f;
}
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0 || section == 1) {
        return 4;
    }
    return 1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return   @"技术能力";
            break;
        case 1:
            return   @"服务状态";
            break;
        case 2:
            return  @"签收意见";
            break;
        default:
            break;
    }
    return nil;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    if ([indexPath section] == 0 || [indexPath section] == 1) {
        cell.textLabel.text = [self.titles objectAtIndex:[indexPath row]];
        if ([indexPath row] == 0) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
    }
    if ([indexPath section] == 2) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        self.contentTextField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 320, 80)];
        self.contentTextField.delegate = self;
        self.contentTextField.placeholder = @"签收意见";
        [cell.contentView addSubview:self.contentTextField];
        
        
    }
    // Configure the cell...
    
    return cell;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([indexPath section] == 0) {
        if (accflagCurrentIndex == indexPath.row) {
            return;
        }
        
        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType  = UITableViewCellAccessoryCheckmark;
        if (accflagCurrentIndex >= 0) {
            NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:accflagCurrentIndex inSection:0];
            UITableViewCell *oldCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
            oldCell.accessoryType  = UITableViewCellAccessoryNone;
        }
        accflagCurrentIndex = [indexPath row];

    }
    if ([indexPath section] == 1) {
        if (manflagCurrentIndex == indexPath.row) {
            return;
        }
        
        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType  = UITableViewCellAccessoryCheckmark;
        if (manflagCurrentIndex >= 0) {
            NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:manflagCurrentIndex inSection:1];
            UITableViewCell *oldCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
            oldCell.accessoryType  = UITableViewCellAccessoryNone;
        }
        manflagCurrentIndex = [indexPath row];
        
    }

}

@end

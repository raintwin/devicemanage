//
//  MaintainTraderItem.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MaintainTraderItem : NSManagedObject

@property (nonatomic, retain) NSNumber * repairId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * telephone;
@property (nonatomic, retain) NSString * contact;

@end

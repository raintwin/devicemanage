//
//  LoanDeviceViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-9.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "rootViewController.h"
@interface LoanDeviceViewController : ViewController<UITextFieldDelegate,UIActionSheetDelegate,ZBarReaderDelegate>


@property (nonatomic, weak) IBOutlet UITextField *deviceNoTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceTypeTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceBrandTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceModelTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceDateTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceDepTextField;
@property (nonatomic, weak) IBOutlet UITextField *deviceLoanerTextField;


@property (nonatomic, weak) IBOutlet UIButton * scanButton;
@property (nonatomic, weak) IBOutlet UIButton * dateButton;
@property (nonatomic,retain) NSNumber *deviceId;
- (IBAction)scanBarcode:(id)sender;
@end

//
//  Type.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "Type.h"


@implementation Type

@dynamic type_id;
@dynamic category_id;
@dynamic name;
@dynamic modify_date;

@end

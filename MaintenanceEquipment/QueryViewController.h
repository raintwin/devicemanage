//
//  QueryViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-31.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QueryViewControllerDelegate <NSObject>
@optional
-(void)queryDictiony:(NSMutableDictionary *)dict;
@end

@interface QueryViewController : UIViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

{
    int currentIndex;

}
@property (weak, nonatomic) IBOutlet UIView *deviceView;
@property (weak, nonatomic) IBOutlet UIView *loanView;
@property (weak, nonatomic) IBOutlet UIView *mianView;


@property (weak, nonatomic) IBOutlet UITextField *deviceNoTextField;
@property (weak, nonatomic) IBOutlet UITextField *deviceNameTextField;

@property (weak, nonatomic) IBOutlet UIButton * queryButton;


@property (nonatomic, weak) IBOutlet UITableView *loanTableView;
@property (nonatomic, weak) IBOutlet UITableView *mainTableView;

-(NSMutableDictionary *)getDateDictionary;
-(NSMutableDictionary *)getMianRecodeDateDictionary;
@property (assign ,nonatomic) id<QueryViewControllerDelegate>delegate;
- (IBAction)queryButton:(id)sender;

@end

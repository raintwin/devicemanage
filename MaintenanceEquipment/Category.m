//
//  Category.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-22.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "Category.h"


@implementation Category

@dynamic category_id;
@dynamic modify_date;
@dynamic name;
@dynamic material_no;
@dynamic brand_id;
@dynamic type_id;
@dynamic material_id;
@dynamic price;
@dynamic flag;

@end

//
//  maintaintraderViewCotroller.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "rootViewController.h"
#import "MaintainTraderItem.h"

@protocol MaintaintraderViewCotrollerDelegate <NSObject>
@optional

- (void) maintainTraderInfo:(MaintainTraderItem *)maintainTraderItem;

@end

@interface maintaintraderViewCotroller : ViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{

}
@property(nonatomic,weak) id<MaintaintraderViewCotrollerDelegate>Delegate;
@property (nonatomic, assign) BOOL isDidSelect;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;


@end

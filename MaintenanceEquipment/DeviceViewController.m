//
//  DeviceViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-3.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "DeviceViewController.h"
#import "EquipView.h"
#import "DeviceViewCell.h"
#import <QuartzCore/QuartzCore.h>

#define Height 105.0f


static NSString *const DeviceCellIdentifier = @"DeviceCellIdentifier";


@interface DeviceViewController ()
@property(nonatomic,retain) NSMutableArray *rowToInserts;
@property(nonatomic,retain) NSString *barcodeString;
@property BOOL isQuery;
@end

@implementation DeviceViewController
@synthesize tableView = _tableView;
@synthesize rowToInserts;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize barcodeString;

typedef enum _DeviceStatus {
    
    STATUS_NORMAL = 1,
    STATUS_NO ,
    STATUS_MAINTAIN,
    STATUS_LOAN
}DeviceStatus;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.rootDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    didSectionFlag  = -1;
    isOpenCell = NO;
    [self.rootDelegate.viewController requestEquipItem];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self setDeviceItemRightBarButtonItems];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"DeviceViewCell" bundle:nil] forCellReuseIdentifier:DeviceCellIdentifier];

    self.barcodeString = nil;
    self.queryDict = nil;
    [self setTipViewForTabelView:self.tableView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}
#pragma mark 刷新
- (void)startLoading {
    self.barcodeString = nil;
    self.queryDict = nil;
    self.fetchedResultsController = nil;
    self.fetchedResultsController.delegate = nil;
    [self.tableView reloadData];

    [self.rootDelegate.viewController requestEquipItem];
}

#pragma mark 查询
-(void) queryData
{
    CGRect queryViewRect = [self.view bounds];
    queryViewRect.origin.y = -500;
    
    if (self.queryViewController != nil) {
        queryViewRect.origin.y = 0;
        [UIView animateWithDuration:0.5 animations:^{
            self.queryViewController.view.frame = queryViewRect;
        }];
        return;
    }

    self.queryViewController = [[QueryViewController alloc]initWithNibName:@"QueryViewController" bundle:nil];
    self.queryViewController.delegate = self;
    [self.queryViewController.view setFrame:queryViewRect];
    self.queryViewController.deviceView.hidden = NO;
    [self.view addSubview:self.queryViewController.view];
    
    queryViewRect.origin.y = 0;
    [UIView animateWithDuration:0.5 animations:^{
        self.queryViewController.view.frame = queryViewRect;
    }];

    
}
-(void)queryDictiony:(NSMutableDictionary *)dict;
{
    CGRect queryViewRect = [self.view bounds];
    queryViewRect.origin.y = -500;
    [UIView animateWithDuration:0.5 animations:^{
        self.queryViewController.view.frame = queryViewRect;
    }];
    self.queryDict = dict;
    NSLog(@"self.queryDict=%@",self.queryDict);
    [self queryOfDictionary:self.queryDict];

}
-(void)querySQL
{
    
}
#pragma mark 扫描
-(void) scanningDevice
{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    reader.navigationController.navigationBar.hidden = YES;
    reader.showsZBarControls = NO;
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
//    [self presentModalViewController: reader
//                            animated: YES];

    [self.navigationController pushViewController:reader animated:YES];
    self.navigationController.toolbarHidden = YES;

}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
//    resultText.text = symbol.data;
    
    NSLog(@"symbol.data =%@",symbol.data);
    [self deviceItemOfBarcode:symbol.data];
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)deviceItemOfBarcode:(NSString *)barcode
{
    
    self.barcodeString = barcode;
    [self performSelectorOnMainThread:@selector(queryBarcode) withObject:nil waitUntilDone:YES];
}

#pragma mark --
- (void)queryBarcode {
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deviceNo"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"barcode == %@",self.barcodeString];
    [request setPredicate:predicate];
    [request setFetchBatchSize:20];
    
    NSError *error = nil;
    //执行获取数据请求，返回数组fdeviceId
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    if ([mutableFetchResult count] > 0) {
        self.fetchedResultsController = nil;
        self.fetchedResultsController.delegate = nil;
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"条形码" message:self.barcodeString delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSString *messageString = [NSString stringWithFormat:@"条形码:%@ 没有对应的设备",self.barcodeString];
        self.barcodeString = nil;
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:messageString delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
- (void)queryOfDictionary:(NSMutableDictionary *)dict {
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deviceNo"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    NSString *deviceNo = [dict objectForKey:@"deviceNo"];
    if ([deviceNo length] > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceNo CONTAINS[cd] %@",deviceNo];
        [request setPredicate:predicate];
        [request setFetchBatchSize:20];
    }
    

    
    NSError *error = nil;
    //执行获取数据请求，返回数组fdeviceId
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    if ([mutableFetchResult count] > 0) {
        NSLog(@"success count=%d",[mutableFetchResult count]);
        
        self.fetchedResultsController = nil;
        self.fetchedResultsController.delegate = nil;
        [self.tableView reloadData];
    }
    else
    {
        self.queryDict = nil;
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"没有对应的设备" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
//    self.barcodeString = nil;

}
#pragma mark 设备借出登记
-(void)loanEquipment:(UIButton *)sender
{
    NSLog(@"sender :%d",sender.tag);
    NSString *nibName = NSStringFromClass([LoanDeviceViewController class]);
    LoanDeviceViewController *DeviceViewController = [[LoanDeviceViewController alloc]initWithNibName:nibName bundle:nil];
    DeviceViewController.deviceId = [NSNumber numberWithInteger:sender.tag];
    
    DeviceViewController.title = @"设备借出登记";
    [self.navigationController pushViewController:DeviceViewController animated:YES];
    [DeviceViewController setNavgationBack];

}
#pragma mark 报修
- (void)maintainEquipment:(UIButton *)sender
{
    NSString *nibName = NSStringFromClass([MaintainViewController class]);
    MaintainViewController *maintainViewController = [[MaintainViewController alloc]initWithNibName:nibName bundle:nil];
    maintainViewController.title = @"设备维修申请";
    maintainViewController.deviceId = [NSNumber numberWithInteger:sender.tag];
    [self.navigationController pushViewController:maintainViewController animated:YES];
    [maintainViewController setNavgationBack];

}
#pragma mark 归还设备
- (void)revertDevice:(UIButton *)sender
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    //    [dict setObject:[NSNumber numberWithInt:sender.tag] forKey:@""]
}

#pragma mark

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeadHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;   // custom view for header. will be adjusted to default or specified header height
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:section inSection:0];
    DeviceItem *deviceItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    EquipView *equipView = [[EquipView alloc]initWithFrame:CGRectMake(0, 0, 320, HeadHeight)];
    equipView.equipNameLabel.text = deviceItem.deviceName;
    equipView.equipIdLabel.text = deviceItem.deviceNo;
    [self setDeviceStatus:equipView deviceStatus:[deviceItem.deviceStatus integerValue] index:deviceItem.deviceId];
    equipView.popupCellButton.tag = section;
    [equipView.popupCellButton addTarget:self action:@selector(popupCell:) forControlEvents:64];
    
    return equipView;
}
-(void)popupCell:(UIButton *)sender
{
    
    if (isOpenCell) {
        NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:didSectionFlag];
        [rowToInsert addObject:indexPath];
        didSectionFlag = -1;

        [self.tableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];

        isOpenCell = NO;
    }else{
        didSectionFlag = sender.tag;
        NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:didSectionFlag];
        [rowToInsert addObject:indexPath];
        [self.tableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
        isOpenCell = YES;
    }
    
    [self.tableView endUpdates];
}
- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert{
    [self.tableView beginUpdates];
    isOpenCell = firstDoInsert;
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:didSectionFlag];
    [rowToInsert addObject:indexPath];
    if (!isOpenCell) {
        didSectionFlag = -1;
        [self.tableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }else{
        [self.tableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
    [self.tableView endUpdates];
    //    if (nextDoInsert) {
    //        didSectionFlag = oldSectionFlag;
    //        [self didSelectCellRowFirstDo:YES nextDo:NO];
    //    }
    [self.tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}
-(void)setDeviceStatus:(EquipView *)equipView deviceStatus:(int)deviceStatus index:(NSNumber *)deviceId
{
    switch (deviceStatus) {
        case STATUS_NORMAL:
            equipView.loanButton.hidden = NO;
            equipView.maintainButton.hidden = NO;
            equipView.loanButton.tag = [deviceId integerValue];
            [equipView.loanButton addTarget:self action:@selector(loanEquipment:) forControlEvents:64];
            
            equipView.maintainButton.tag = [deviceId integerValue];
            [equipView.maintainButton addTarget:self action:@selector(maintainEquipment:) forControlEvents:64];
            break;
        case STATUS_NO:
            equipView.badButton.hidden = NO;
            [equipView.badButton setEnabled:NO];
            break;
        case STATUS_MAINTAIN:
            equipView.maintainingButton.hidden = NO;
            equipView.maintainingButton.enabled = NO;
            break;
        case STATUS_LOAN:
            equipView.revertButton.hidden = NO;
            equipView.revertButton.enabled = NO;
            equipView.tag = [deviceId intValue];
            [equipView.revertButton addTarget:self action:@selector(revertDevice:) forControlEvents:64];
            break;
            
        default:
            break;
    }
}
#pragma mark
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Height;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[[self.fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
//    return [[self.fetchedResultsController sections] count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (didSectionFlag == section) {
        return 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *inSectionIndaxPath = [NSIndexPath indexPathForRow:[indexPath section] inSection:0];
    DeviceItem *item = [self.fetchedResultsController objectAtIndexPath:inSectionIndaxPath];
    NSLog(@"item :%@",item.deviceName);
    DeviceViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:DeviceCellIdentifier];
    [cell populateCellWithEvent:item];
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
//    DeviceItem *DeviceItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    cell.textLabel.text = DeviceItem.deviceName;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark -- NSFetchedResultsController
- (NSFetchedResultsController *)fetchedResultsController
{
    if (nil != _fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *playerEntity = [NSEntityDescription entityForName:@"DeviceItem" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    [fetchRequest setEntity:playerEntity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"deviceNo"ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSPredicate *predicate = nil;

    if (self.barcodeString != nil && [self.barcodeString length] > 0) {
        predicate = [NSPredicate predicateWithFormat:@"barcode == %@",self.barcodeString];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setFetchBatchSize:20];
     }
    if (self.queryDict != nil) {
        NSString *deviceNo = [self.queryDict objectForKey:@"deviceNo"];
        NSString *deviceName = [self.queryDict objectForKey:@"deviceName"];
        if ([deviceNo length] > 0 ) {
            predicate = [NSPredicate predicateWithFormat:@"deviceNo CONTAINS[cd] %@",deviceNo];
        }
        if ([deviceName length] > 0 ) {
            predicate = [NSPredicate predicateWithFormat:@"deviceName CONTAINS[cd] %@",deviceName];
        }
        if ([deviceName length] > 0 && [deviceNo length] > 0) {
            predicate = [NSPredicate predicateWithFormat:@"(deviceNo CONTAINS[cd] %@) and (deviceName CONTAINS[cd] %@)",deviceNo,deviceName];
        }
        [fetchRequest setPredicate:predicate];
        [fetchRequest setFetchBatchSize:20];

    }
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.rootDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    
    NSError *error = NULL;
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _fetchedResultsController;
}
#pragma mark --

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
    self.barcodeString = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

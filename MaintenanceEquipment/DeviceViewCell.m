//
//  DeviceViewCell.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-12.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "DeviceViewCell.h"

@implementation DeviceViewCell
@synthesize brandLabel;
@synthesize oneClassLabel;
@synthesize twoClassLabel;
@synthesize priceLabel;
@synthesize dateLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)populateCellWithEvent:(DeviceItem *)item;
{
    self.brandLabel.text = item.brand;
    self.oneClassLabel.text = item.category;
    self.twoClassLabel.text = item.type;
    self.priceLabel.text = [item.price stringValue];
    self.dateLabel.text = item.buyDate;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

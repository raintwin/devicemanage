//
//  RepairerItemViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-16.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "RepairerItemViewController.h"
#import "RepairerItemCell.h"

static NSString *const RepairerItemCellIdentifier = @"RepairerItemCellIdentifier";


@interface RepairerItemViewController ()

@end

@implementation RepairerItemViewController
@synthesize tableView = _tableView;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize isDelegate;
@synthesize maintainRecordItem;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.rootDelegate.viewController requestRepairerItem];
    [self.tableView registerNib:[UINib nibWithNibName:@"RepairerItemCell" bundle:nil] forCellReuseIdentifier:RepairerItemCellIdentifier];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    currentIndex = -1;
    
    if (isDelegate) {
        UIImage *imageBtn = [UIImage imageNamed:@"3355_05"];
        UIImage *backgroundButtonImage = [UIImage imageNamed:@"3355_05"];
        backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                           topCapHeight:backgroundButtonImage.size.height/2];
        
        CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
        
        UIButton *subitButton = [[UIButton alloc] initWithFrame:buttonRect];
        [subitButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
        [subitButton setTitle:@"确定" forState:UIControlStateNormal];
        subitButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        subitButton.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
        [subitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [subitButton addTarget:self action:@selector(delegateRepairer) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *subitItem = [[UIBarButtonItem alloc] initWithCustomView:subitButton];
        self.navigationItem.rightBarButtonItem = subitItem;

    }
        
}
#pragma mark 委派
-(void)delegateRepairer
{
    if (currentIndex < 0) {
        UIAlertView  *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请选委派的维修员" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    NSLog(@"maintenanceId =%d",[maintainRecordItem.maintenanceId intValue]);

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    Repairer *item = [self.fetchedResultsController objectAtIndexPath:indexPath];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:item.userId forKey:@"repairPersonId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];

    [dict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [dict setObject:self.maintainRecordItem.maintenanceId forKey:@"maintenanceId"];
    [dict setObject:REQ_DELEGATEREPAIR forKey:KEY_DREQTYPE];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_DELEGATEREPAIRER];
    [[NSUserDefaults standardUserDefaults] setObject:self.maintainRecordItem.deviceNo forKey:DEFDEVICENO];
}
#pragma mark
#pragma mark 
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 35.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;   // custom view for header. will be adjusted to default or specified header height
{
    UIImage *image = [UIImage imageNamed:@"维修员_02"];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [imageView setImage:image];
    return imageView;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 1) {
        [self requestCancelOperation:alertView.tag];
    }
}

#pragma mark
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Repairer *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    RepairerItemCell *cell = [self.tableView dequeueReusableCellWithIdentifier:RepairerItemCellIdentifier];
    cell.isMark = self.isDelegate;
    [cell populateCellWithEvent:item];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}


-(void)requestCancelOperation:(int)indexRow
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexRow inSection:0];
    MaintainRecordItem *recordItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] forKey:@"userName"];
    [dict setObject:recordItem.maintenanceId forKey:@"maintenanceId"];
    [dict setObject:REQ_MAINTAINCANCELECORD forKey:KEY_DREQTYPE];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_MAINTAINTCANCEL_CODE];
}

#pragma mark -----

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currentIndex == indexPath.row) {
        return;
    }

    RepairerItemCell *cell = (RepairerItemCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType  = UITableViewCellAccessoryCheckmark;
    if (currentIndex >= 0) {
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        RepairerItemCell *oldCell = (RepairerItemCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
        oldCell.accessoryType  = UITableViewCellAccessoryNone;
    }
    currentIndex = [indexPath row];
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (nil != _fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *playerEntity = [NSEntityDescription entityForName:@"Repairer" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    [fetchRequest setEntity:playerEntity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"userId"ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.rootDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:@"Repairer"];
    _fetchedResultsController.delegate = self;
    
    NSError *error = NULL;
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _fetchedResultsController;
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller;
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

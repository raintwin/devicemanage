//
//  MaintainRecordViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-3.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MaintainRecordViewController.h"
#import "EquipView.h"
#import "LoanItemCell.h"
#import "MainRecordInofViewController.h"

#define Height 105.0f


static NSString *const MaintainRecordCellIdentifier = @"MaintainRecordCellellIdentifier";


@interface MaintainRecordViewController ()
@property(nonatomic,retain) NSMutableArray *rowToInserts;
@end

@implementation MaintainRecordViewController
@synthesize tableView = _tableView;
@synthesize rowToInserts;
@synthesize fetchedResultsController = _fetchedResultsController;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.rootDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
//    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
//    [dict setObject:REQ_MAINTAINRECORD forKey:KEY_DREQTYPE];
    
    [self.rootDelegate.viewController requestMaintainRecord];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MaintainRecordCell" bundle:nil] forCellReuseIdentifier:MaintainRecordCellIdentifier];

    [self setDeviceItemRightBarOfquery];
    [self setTipViewForTabelView:self.tableView];

}
#pragma mark 刷新
- (void)startLoading {
    [self.rootDelegate.viewController requestMaintainRecord];
}

#pragma mark 查询
-(void) queryData
{
    CGRect queryViewRect = [self.view bounds];
    
    queryViewRect.origin.y = -500;
    
    if (self.queryViewController != nil) {
        if ([self.queryViewController getDateDictionary] != nil) {
            self.queryDict = [self.queryViewController getMianRecodeDateDictionary];
            [self queryBaseData];
            
        }
        queryViewRect.origin.y =  -500;
        [UIView animateWithDuration:0.5 animations:^{
            self.queryViewController.view.frame = queryViewRect;
        } completion:^(BOOL finished){
            [self.queryViewController.view  removeFromSuperview];
            self.queryViewController = nil;
        }];
        return;
    }
    
    self.queryViewController = [[QueryViewController alloc]initWithNibName:@"QueryViewController" bundle:nil];
    [self.queryViewController.view setFrame:queryViewRect];
    self.queryViewController.mianView.hidden = NO;
    self.queryViewController.delegate = self;
    [self.view addSubview:self.queryViewController.view];
    
    queryViewRect.origin.y = 0;
    [UIView animateWithDuration:0.5 animations:^{
        self.queryViewController.view.frame = queryViewRect;
    }];
    
    
}
-(void)queryDictiony:(NSMutableDictionary *)dict;
{
    
    CGRect queryViewRect = [self.view bounds];
    queryViewRect.origin.y = -500;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.queryViewController.view.frame = queryViewRect;
    } completion:^(BOOL finished){
        [self.queryViewController.view  removeFromSuperview];
        self.queryViewController = nil;
    }];
    
    if (dict == nil) {
        return;
    }
}
-(void)queryBaseData
{
    NSLog(@"self.queryData=%@",self.queryDict);
    
    [self.queryDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [self.queryDict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [self.queryDict setObject:[analysisJson getCharterReq] forKey:KEY_DREQTYPE];
    
    [self.queryDict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [self.queryDict setObject:[NSNumber numberWithInt:1000] forKey:@"everyPage"];
    
    [self.rootDelegate.viewController generalRequest:self.queryDict tag:REQUESTED_MAINTAINRECORD_CODE];

    
}
#pragma mark
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

#pragma mark
#pragma mark
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Height;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MaintainRecordItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    MaintainRecordCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MaintainRecordCellIdentifier];
    [cell populateCellWithEvent:item];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.OptionButton.tag = [indexPath row];
    [cell.OptionButton addTarget:self action:@selector(cancelOperation:) forControlEvents:64];
    return cell;
}

#pragma mark 撤销操作

-(void)cancelOperation:(UIButton *)sender
{
    switch ([analysisJson getCharterType]) {
        case CTSCHOOLTYPE:
        case CTSCHOOLAPPLYTYPE:
        {    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"是否撤销申请？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alertView.tag  = sender.tag;
            alertView.delegate = self;
            [alertView show];
        }
            break;
        case CTREAPAIRTYPE:
            [self delegateRepairer:sender.tag];
            break;
        default:
            break;
    }
    

    return;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 1) {
        [self requestCancelOperation:alertView.tag];
    }
}
-(void)requestCancelOperation:(int)indexRow
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexRow inSection:0];
    MaintainRecordItem *recordItem = [self.fetchedResultsController objectAtIndexPath:indexPath];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] forKey:@"userName"];
    [dict setObject:recordItem.maintenanceId forKey:@"maintenanceId"];
    [dict setObject:REQ_MAINTAINCANCELECORD forKey:KEY_DREQTYPE];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_MAINTAINTCANCEL_CODE];
    [[NSUserDefaults standardUserDefaults] setObject:recordItem.deviceNo forKey:DEFDEVICENO];

}
#pragma mark 委派
-(void)delegateRepairer:(int)currentIndex;
{
    if (currentIndex < 0) {
        return;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    MaintainRecordItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];

    RepairerItemViewController *maintenanceViewController = [[RepairerItemViewController alloc]initWithNibName:@"RepairerItemViewController" bundle:nil];
    maintenanceViewController.maintainRecordItem = item;
    maintenanceViewController.isDelegate = YES;
    [self.navigationController pushViewController:maintenanceViewController animated:YES];
    [maintenanceViewController setNavgationBack];
}
#pragma mark -----

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MaintainRecordItem *recordItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    MainRecordInofViewController *mainRecordInofViewController = [[MainRecordInofViewController alloc]initWithNibName:@"MainRecordInofViewController" bundle:nil];
    mainRecordInofViewController.maintainRecordItem = recordItem;
    mainRecordInofViewController.title = recordItem.deviceName;

    [self.navigationController pushViewController:mainRecordInofViewController animated:YES];
    [mainRecordInofViewController setNavgationBack];
}

#pragma mark 
- (NSFetchedResultsController *)fetchedResultsController
{
    if (nil != _fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *playerEntity = [NSEntityDescription entityForName:@"MaintainRecordItem" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    [fetchRequest setEntity:playerEntity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"modifyDate"ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"team == %@",@"dd"];
    //    [fetchRequest setPredicate:predicate];
    //    [fetchRequest setFetchBatchSize:20];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.rootDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:@"MaintainRecordItem"];
    _fetchedResultsController.delegate = self;
    
    NSError *error = NULL;
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _fetchedResultsController;
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

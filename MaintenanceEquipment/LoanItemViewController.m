//
//  LoanItemViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-3.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "LoanItemViewController.h"
#import "EquipView.h"
#import "LoanItemCell.h"
#define Height 105.0f


static NSString *const LoanItemCellIdentifier = @"LoanItemCellIdentifier";


@interface LoanItemViewController ()
@property(nonatomic,retain) NSMutableArray *rowToInserts;
@end

@implementation LoanItemViewController
@synthesize tableView = _tableView;
@synthesize rowToInserts;
@synthesize fetchedResultsController = _fetchedResultsController;


typedef enum _DeviceStatus {
    
    STATUS_NORMAL = 1,
    STATUS_NO ,
    STATUS_MAINTAIN,
    STATUS_LOAN
}DeviceStatus;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.rootDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    didSectionFlag  = -1;
    isOpenCell = NO;
    self.queryDict = nil;
    [self setDeviceItemRightBarOfquery];
    [self.rootDelegate.viewController requestLoanItem];
    [self setTipViewForTabelView:self.tableView];

}
#pragma mark 刷新
- (void)startLoading {
    
    
    [self.rootDelegate.viewController requestLoanItem];
}


#pragma mark 查询
-(void) queryData
{
    CGRect queryViewRect = [self.view bounds];
    
    queryViewRect.origin.y = -500;
    
    if (self.queryViewController != nil) {
        if ([self.queryViewController getDateDictionary] != nil) {
            self.queryDict = [self.queryViewController getDateDictionary];
            [self queryLoanRecorde];
        }
        queryViewRect.origin.y =  -500;
        [UIView animateWithDuration:0.5 animations:^{
            self.queryViewController.view.frame = queryViewRect;
        } completion:^(BOOL finished){
            [self.queryViewController.view  removeFromSuperview];
            self.queryViewController = nil;
        }];
        return;
    }
    
    self.queryViewController = [[QueryViewController alloc]initWithNibName:@"QueryViewController" bundle:nil];
    [self.queryViewController.view setFrame:queryViewRect];
    self.queryViewController.loanView.hidden = NO;
    self.queryViewController.delegate = self;
    [self.view addSubview:self.queryViewController.view];
    
    queryViewRect.origin.y = 0;
    [UIView animateWithDuration:0.5 animations:^{
        self.queryViewController.view.frame = queryViewRect;
    }];
    
    
}
-(void)queryDictiony:(NSMutableDictionary *)dict;
{

    CGRect queryViewRect = [self.view bounds];
    queryViewRect.origin.y = -500;

    [UIView animateWithDuration:0.5 animations:^{
        self.queryViewController.view.frame = queryViewRect;
    } completion:^(BOOL finished){
        [self.queryViewController.view  removeFromSuperview];
        self.queryViewController = nil;
    }];
    
    if (dict == nil) {
        return;
    }
}
#pragma mark 
#pragma mark -- 查询借出记录
- (void)queryLoanRecorde {
    NSLog(@"self.queryDict =%@",self.queryDict);
    [self.rootDelegate.viewController generalRequest:self.queryDict tag:REQUESTED_LOANITEM_CODE];
}

#pragma mark 
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LoanItemCell" bundle:nil] forCellReuseIdentifier:LoanItemCellIdentifier];
    
}

#pragma mark

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeadHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;   // custom view for header. will be adjusted to default or specified header height
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:section inSection:0];
    LoanDeviceItem *deviceItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    EquipView *equipView = [[EquipView alloc]initWithFrame:CGRectMake(0, 0, 320, HeadHeight)];
    equipView.equipNameLabel.text = deviceItem.deviceName;
    equipView.equipIdLabel.text = deviceItem.deviceNo;
    [self setDeviceStatus:equipView deviceStatus:[deviceItem.deviceStatus integerValue] index:section];
    equipView.popupCellButton.tag = section;
    [equipView.popupCellButton addTarget:self action:@selector(popupCell:) forControlEvents:64];
    
    return equipView;
}
-(void)popupCell:(UIButton *)sender
{
    
    if (isOpenCell) {
        NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:didSectionFlag];
        [rowToInsert addObject:indexPath];
        didSectionFlag = -1;
        
        [self.tableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
        
        isOpenCell = NO;
    }else{
        didSectionFlag = sender.tag;
        NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:didSectionFlag];
        [rowToInsert addObject:indexPath];
        [self.tableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
        isOpenCell = YES;
    }
    
    [self.tableView endUpdates];
}

-(void)setDeviceStatus:(EquipView *)equipView deviceStatus:(BOOL)deviceStatus index:(NSInteger )index
{
    //deviceStatus true 已归还
    if (deviceStatus) {
        equipView.didReverButton.hidden = NO;
        [equipView.didReverButton setEnabled:NO];
    }
    else
    {
        equipView.revertButton.hidden = NO;
        equipView.revertButton.tag = index;
        [equipView.revertButton addTarget:self action:@selector(revertDevice:) forControlEvents:64];
    }
}


#pragma mark 归还设备
- (void)revertDevice:(UIButton *)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定归还该设备" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag  = sender.tag;
    alertView.delegate = self;
    [alertView show];
    return;
}
#pragma mark 
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 1) {
        NSIndexPath *inSectionIndaxPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        LoanDeviceItem *item = [self.fetchedResultsController objectAtIndexPath:inSectionIndaxPath];
        NSLog(@"item item.deviceNo=:%@",item.deviceNo);
        [self requestReverDevice:item];
    }
}
-(void)requestReverDevice:(LoanDeviceItem *)item
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:item.landOutId forKey:@"landOutId"];
    [dict setObject:REQ_REVERT forKey:KEY_DREQTYPE];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_REVERTDEVICE_DODE];
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:item.deviceNo forKey:DEFDEVICENO];
}
#pragma mark
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Height;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[[self.fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
    //    return [[self.fetchedResultsController sections] count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (didSectionFlag == section) {
        return 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *inSectionIndaxPath = [NSIndexPath indexPathForRow:[indexPath section] inSection:0];
    LoanDeviceItem *item = [self.fetchedResultsController objectAtIndexPath:inSectionIndaxPath];
    LoanItemCell *cell = [self.tableView dequeueReusableCellWithIdentifier:LoanItemCellIdentifier];
    [cell populateCellWithEvent:item];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark -- 

- (NSFetchedResultsController *)fetchedResultsController
{
    if (nil != _fetchedResultsController) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *playerEntity = [NSEntityDescription entityForName:@"LoanDeviceItem" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    [fetchRequest setEntity:playerEntity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"deviceStatus"ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"team == %@",@"dd"];
    //    [fetchRequest setPredicate:predicate];
    //    [fetchRequest setFetchBatchSize:20];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.rootDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:@"LoanDeviceItem"];
    _fetchedResultsController.delegate = self;
    
    NSError *error = NULL;
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _fetchedResultsController;
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  UIFont+FSPExtensions.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-5.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//


static NSString *const FSPUScoreRGKFontName = @"UScoreRGK";
static NSString *const FSPUScoreRGHFontName = @"UScoreRGH";
static NSString *const FSPClearViewRegularFontName = @"ClearViewText";
static NSString *const FSPClearViewMediumFontName = @"ClearViewText";
static NSString *const FSPClearViewHeavyFontName = @"ClearViewText";
static NSString *const FSPClearViewBoldFontName = @"ClearViewText";

static NSString *const FSPClearFOXGothicMediumFontName = @"ClearFOXGothic-Medium";
static NSString *const FSPClearFOXGothicBoldFontName = @"ClearFOXGothic-Bold";
static NSString *const FSPClearFOXGothicHeavyFontName = @"ClearFOXGothic-Heavy";

static NSString *const FSPTheGreatEscapeBoldFontName = @"TheGreatEscape-Bold";

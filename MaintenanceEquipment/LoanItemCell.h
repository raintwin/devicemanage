//
//  LoanItemCell.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-12.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoanDeviceItem.h"
@interface LoanItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *loanerLabel;
@property (weak, nonatomic) IBOutlet UILabel *departLabel;
@property (weak, nonatomic) IBOutlet UILabel *loadDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *retureDateLabel;


- (void)populateCellWithEvent:(LoanDeviceItem *)item;

@end

//
//  LoanDeviceItem.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-8-2.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LoanDeviceItem : NSManagedObject

@property (nonatomic, retain) NSString * deptName;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * deviceNo;
@property (nonatomic, retain) NSNumber * deviceStatus;
@property (nonatomic, retain) NSNumber * landOutId;
@property (nonatomic, retain) NSString * lendDate;
@property (nonatomic, retain) NSString * lendUser;
@property (nonatomic, retain) NSString * returnDate;

@end

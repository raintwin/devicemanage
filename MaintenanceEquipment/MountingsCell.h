//
//  MountingsCell.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-22.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MountingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberPriceLabel;

- (void)populateCellWithEvent:(NSMutableDictionary *)dicionary;

@end

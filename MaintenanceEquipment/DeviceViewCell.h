//
//  DeviceViewCell.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-12.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DeviceItem.h"
@interface DeviceViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;



- (void)populateCellWithEvent:(DeviceItem *)item;

@end

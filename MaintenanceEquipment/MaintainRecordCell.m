//
//  MaintainRecordCell.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-15.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MaintainRecordCell.h"


typedef enum _DeviceStatus {
    
    STATUS_APPLYING = 0,
    STATUS_MAINTAINING ,
    STATUS_WAITING,
    STATUS_COMPLETE,
    STATUS_CANCEL
}DeviceStatus;

@implementation MaintainRecordCell
@synthesize numberLabel;
@synthesize deviceStatusLabel;
@synthesize deviceNoLabel;
@synthesize applyDataLabel;
@synthesize OptionButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.numberLabel.text = @"";
    self.deviceStatusLabel.text = @"";
    self.deviceNoLabel.text = @"";
    self.applyDataLabel.text = @"";
    self.OptionButton.hidden = YES;
}
- (void)populateCellWithEvent:(MaintainRecordItem *)item;
{
    self.numberLabel.text = [item.maintenanceId stringValue];
    self.deviceStatusLabel.text = item.deviceName;
    self.deviceNoLabel.text = item.deviceNo;
    self.applyDataLabel.text = item.time;
    [self setDeviceStatus:item.status];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDeviceStatus:(NSNumber *)deviceId
{
    //deviceStatus true 已归还
    self.OptionButton.hidden = YES;
    NSString *statusString = nil;
    switch ([deviceId integerValue]) {
        case STATUS_APPLYING:
            statusString = @"申请中";
            self.OptionButton.hidden = NO;
            if ([analysisJson getCharterType] == CTSCHOOLTYPE || [analysisJson getCharterType] == CTSCHOOLAPPLYTYPE)  {
                [self.OptionButton setTitle:@"撤销" forState:UIControlStateNormal];
            }else if([analysisJson getCharterType] == CTREAPAIRTYPE)
            {
                [self.OptionButton setTitle:@"委派" forState:UIControlStateNormal];
            }
            break;
            
        case STATUS_MAINTAINING:
            statusString = @"维修中";
            break;
            
        case STATUS_WAITING:
            statusString = @"完成 待签收";
            break;
            
        case STATUS_COMPLETE:
            statusString = @"完成 已签收";
            break;
            
        case STATUS_CANCEL:
            statusString = @"已撤销";
            break;
            
        default:
            break;
    }
    self.deviceStatusLabel.text = statusString;
    [self.deviceStatusLabel setTextColor:[UIColor blueColor]];
}
//- (void)setOption
//{
//    self.OptionButton.hidden = NO;
//
//    switch ([analysisJson getCharterType]) {
//        case CTSCHOOLTYPE:
//            [self.OptionButton setTitle:@"撤销" forState:UIControlStateNormal];
//            break;
//        case CTREAPAIRTYPE:
//            [self.OptionButton setTitle:@"委派" forState:UIControlStateNormal];
//            break;
//        default:
//            break;
//    }
//
//}
@end

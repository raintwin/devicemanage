//
//  PartsViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "Category.h"
@protocol PartsViewControllerDelegate <NSObject>

- (void)setDictionary:(Category *)category key:(NSString *)keystring;
@end

@class MountingsViewController;
@interface PartsViewController : ViewController <NSFetchedResultsControllerDelegate>
{
}
@property (nonatomic,assign)id<PartsViewControllerDelegate>delegate;

@property (nonatomic,retain)NSString *keyString;
@property (nonatomic,retain)NSPredicate *predicate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;

@end

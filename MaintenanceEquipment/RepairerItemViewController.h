//
//  RepairerItemViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-16.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "MainRecordInofViewController.h"
#import "ViewController.h"
#import "MaintainRecordItem.h"
@interface RepairerItemViewController : ViewController<NSFetchedResultsControllerDelegate>
{
    int currentIndex;
}
@property (nonatomic,assign) BOOL isDelegate;
@property (nonatomic, strong) MaintainRecordItem *maintainRecordItem;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;

@end

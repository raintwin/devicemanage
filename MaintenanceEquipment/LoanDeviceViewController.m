//
//  LoanDeviceViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-9.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "LoanDeviceViewController.h"

@interface LoanDeviceViewController ()
@property(nonatomic,strong) UIView *dateView;
@property(nonatomic,strong) UIDatePicker *datePicker;
@property(nonatomic,strong) UIToolbar *toolBar;
@end

@implementation LoanDeviceViewController

@synthesize deviceNoTextField     = _deviceNoTextField;
@synthesize deviceTypeTextField   = _deviceTypeTextField;
@synthesize deviceBrandTextField  = _deviceBrandTextField;
@synthesize deviceModelTextField  = _deviceModelTextField;
@synthesize deviceDateTextField   = _deviceDateTextField;
@synthesize deviceDepTextField    = _deviceDepTextField;
@synthesize deviceLoanerTextField = _deviceLoanerTextField;

@synthesize scanButton = _scanButton;
@synthesize dateButton = _dateButton;

@synthesize datePicker;
@synthesize toolBar;
@synthesize dateView;


#define DateViewHeight 304

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.rootDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.deviceNoTextField.delegate = self;
    self.deviceDepTextField.delegate = self;
    self.deviceLoanerTextField.delegate = self;

    
    UIImage *imageBtn = [UIImage imageNamed:@"333333息_05"];
    UIImage *backgroundButtonImage = [UIImage imageNamed:@"333333息_05"];
    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
    
    UIButton *subitButton = [[UIButton alloc] initWithFrame:buttonRect];
    [subitButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [subitButton setTitle:@"提交" forState:UIControlStateNormal];
    subitButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    subitButton.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
    [subitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subitButton addTarget:self action:@selector(submitLoanDevice) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *subitItem = [[UIBarButtonItem alloc] initWithCustomView:subitButton];
    self.navigationItem.rightBarButtonItem = subitItem;
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self queryDeviceInfo];
}

#pragma mark 提交
-(void)submitLoanDevice
{
    for (UIView *subView in [self.view subviews]) {
        if ([subView isKindOfClass:[UITextField class]]) {
            if ([((UITextField *)subView).text length] == 0) {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请输入正确的借出登录信息" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
                return;
            }
        }
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] forKey:@"userName"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [dict setObject:self.deviceDateTextField.text forKey:@"lendOutDate"];
    [dict setObject:self.deviceDepTextField.text forKey:@"deptName"];
    [dict setObject:self.deviceLoanerTextField.text forKey:@"lendUser"];
    [dict setObject:self.deviceId forKey:@"deviceId"];
    [dict setObject:REQ_LOANDDEVICE forKey:KEY_DREQTYPE];
    [self.rootDelegate.viewController generalRequest:dict tag:REQUESTED_SUBMITLOAN_CODE];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.deviceNoTextField.text forKey:DEFDEVICENO];
}
#pragma mark 扫描条形码
- (IBAction)scanBarcode:(id)sender
{
    NSLog(@"打开相机");
    [self scanningDevice];
}
#pragma mark 扫描
-(void) scanningDevice
{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    reader.showsZBarControls = NO;
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    //    [self presentModalViewController: reader
    //                            animated: YES];
    
    [self.navigationController pushViewController:reader animated:YES];
}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    //    resultText.text = symbol.data;
    
    NSLog(@"symbol.data =%@",symbol.data);
    [self queryBarcode:symbol.data];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 日期 
- (IBAction)dateButton:(id)sender
{
    if (self.dateView == nil) {
        self.dateView = [[UIView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, DateViewHeight)];
        if (self.datePicker == nil) {
            self.datePicker = [[UIDatePicker alloc]init];
        }
        self.datePicker.center = self.view.center;
        self.datePicker.datePickerMode=UIDatePickerModeDate;
//        [self.datePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
        self.datePicker.frame = CGRectMake(0,44 , 320, 260);
        
        //创建工具栏
        NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:3];
        UIBarButtonItem *confirmBtn = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(confirmPickView)];
        UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(pickerHide)];
        [items addObject:cancelBtn];
        [items addObject:flexibleSpaceItem];
        [items addObject:confirmBtn];
        if (self.toolBar==nil) {
            self.toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        }
        self.toolBar.hidden = NO;
        self.toolBar.barStyle = UIBarStyleBlackTranslucent;
        self.toolBar.items = items;
        [self.dateView addSubview:self.datePicker];
        [self.dateView addSubview:self.toolBar];
        [self.view addSubview:self.dateView];
    }

    [UIView animateWithDuration:0.5 animations:^{
        self.dateView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - DateViewHeight, 320, DateViewHeight);
    }];
}
- (void)pickerHide
{
    [UIView animateWithDuration:0.5 animations:^{
        self.dateView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, DateViewHeight);
    }];
}
- (void)confirmPickView
{
    NSDate* _date = self.datePicker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *now = [dateFormatter stringFromDate:_date];
//    [dateFormatter setDateFormat:@"HH:mm:ss"];
//    NSString *nowTimer = [dateFormatter stringFromDate:[NSDate date]];
//    now = [NSString stringWithFormat:@"%@ %@",now,nowTimer];
    self.deviceDateTextField.text = [dateFormatter stringFromDate:_date];
    [self pickerHide];
}
#pragma mark --
- (void)queryDeviceNo {
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"barcode"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
   
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceNo == %@",self.deviceNoTextField.text];
    [request setPredicate:predicate];
    [request setFetchBatchSize:20];

    NSError *error = nil;
    //执行获取数据请求，返回数组fdeviceId
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    if ([mutableFetchResult count] > 0) {
        DeviceItem *deviceItem = [mutableFetchResult objectAtIndex:0];
        self.deviceTypeTextField.text = deviceItem.category;
        self.deviceBrandTextField.text = deviceItem.brand;
        self.deviceModelTextField.text = deviceItem.deviceName;
        self.deviceId = deviceItem.deviceId;
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂时该编号设备,请重新输入" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
#pragma mark 查询条形码
#pragma mark --
- (void)queryBarcode:(NSString *)barcode
{
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deviceNo"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"barcode == %@",barcode];
    [request setPredicate:predicate];
    [request setFetchBatchSize:20];
    
    NSError *error = nil;
    //执行获取数据请求，返回数组fdeviceId
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    if ([mutableFetchResult count] > 0) {
        DeviceItem *deviceItem = [mutableFetchResult objectAtIndex:0];
        self.deviceNoTextField.text = deviceItem.deviceNo;
        self.deviceTypeTextField.text = deviceItem.category;
        self.deviceBrandTextField.text = deviceItem.brand;
        self.deviceModelTextField.text = deviceItem.deviceName;
        self.deviceId = deviceItem.deviceId;

    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂时该条形码没有对应的设备" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

#pragma mark --
- (void)queryDeviceInfo {
    //创建取回数据请求
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deviceId"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceId == %@",self.deviceId];
    [request setPredicate:predicate];
    [request setFetchBatchSize:20];
    
    NSError *error = nil;
    //执行获取数据请求，返回数组
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    if ([mutableFetchResult count] > 0) {
        DeviceItem *deviceItem = [mutableFetchResult objectAtIndex:0];
        self.deviceNoTextField.text = deviceItem.deviceNo;
        self.deviceTypeTextField.text = deviceItem.category;
        self.deviceBrandTextField.text = deviceItem.brand;
        self.deviceModelTextField.text = deviceItem.deviceName;
        self.deviceId = deviceItem.deviceId;
    }
}


#pragma mark

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField != self.deviceNoTextField) {
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, -200 , CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        }];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.deviceNoTextField && [textField.text length] > 0) {
        [self queryDeviceNo];
    }

    return YES;
}

#pragma mark -
#pragma mark Responding to keyboard events

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(0, 0 , CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    }];
}


- (void)viewWillAppear:(BOOL)animated;    // Called when the view is about to made visible. Default does nothing
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

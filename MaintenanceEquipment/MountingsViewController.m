//
//  MountingsViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MountingsViewController.h"
#import "PartsViewController.h"
#import "Category.h"
@interface MountingsViewController ()<PartsViewControllerDelegate>
@property(nonatomic ,retain)NSMutableDictionary *dictionary;
@property(nonatomic ,retain)NSMutableDictionary *queryDictionary;
@property(nonatomic ,retain)Category *addCategory;
@end

@implementation MountingsViewController

@synthesize largeButton;
@synthesize typeButton;
@synthesize modelButton;
@synthesize brandButton;

@synthesize priceTextField;
@synthesize numberTextField;

@synthesize dictionary;
@synthesize queryDictionary;
@synthesize addCategory;

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark 配件大类
- (IBAction)largeButton:(id)sender
{
    [self partsViewController:PARTS_CATEGORY];
}
#pragma mark 配件类别
- (IBAction)typeButton:(id)sender
{
    [self partsViewController:PARTS_TYPE];
}
#pragma mark 配件品牌

- (IBAction)modelButton:(id)sender
{
    [self partsViewController:PARTS_MATERIAL];
}
#pragma mark 配件型号
- (IBAction)brandButton:(id)sender
{
    [self partsViewController:PARTS_BRAND];
}

- (void)partsViewController:(PartsType)partsType
{
    NSString *keyString = nil;
    NSString *titleString = nil;
    NSPredicate *predicate = nil;
    switch (partsType) {
        case PARTS_CATEGORY:
            keyString = @"category";
            titleString = self.largeButton.titleLabel.text;
            predicate = [NSPredicate predicateWithFormat:@"flag = %@",keyString];
            break;
        case PARTS_TYPE:
        {
            keyString = @"type";
            titleString = self.typeButton.titleLabel.text;
            NSNumber *category_id = [self.queryDictionary objectForKey:@"category_id"];

            predicate = [NSPredicate predicateWithFormat:@"flag = %@ and category_id = %d",keyString,[category_id intValue]];
        }
            break;
        case PARTS_BRAND:
        {
            keyString = @"brand";
            titleString = self.brandButton.titleLabel.text;
            NSNumber *type_id = [self.queryDictionary objectForKey:@"type_id"];
            
            predicate = [NSPredicate predicateWithFormat:@"flag = %@ and type_id = %d",keyString,[type_id intValue]];
        }
            break;
        case PARTS_MATERIAL:
        {
            keyString = @"material";
            titleString = self.modelButton.titleLabel.text;
            NSNumber *brand_id = [self.queryDictionary objectForKey:@"brand_id"];
            if ([brand_id intValue] >0) {
                predicate = [NSPredicate predicateWithFormat:@"flag = %@ and brand_id = %d",keyString,[brand_id intValue]];
            }
            else
                predicate = [NSPredicate predicateWithFormat:@"flag = %@ ",keyString];

        }
            break;
        default:
            break;
    }
    
    PartsViewController *partsViewController = [[PartsViewController alloc]initWithNibName:@"PartsViewController" bundle:nil];
    partsViewController.title = titleString;
    partsViewController.delegate = self;
    partsViewController.predicate = predicate;
    partsViewController.keyString = keyString;
    [self.navigationController pushViewController:partsViewController animated:YES];
}
#pragma mark delegate 
- (void)setDictionary:(Category *)category key:(NSString *)keystring;
{
    if ([keystring isEqualToString:@"category"]) {
        NSLog(@"category.category_id -%@",[category.category_id stringValue]);
        [self.largeButton setTitle:category.name forState:UIControlStateNormal];
        [self.queryDictionary setObject:category.category_id forKey:@"category_id"];
    }
    else if ([keystring isEqualToString:@"type"])
    {
        NSLog(@"type_id -%@",[category.type_id stringValue]);

        [self.typeButton setTitle:category.name forState:UIControlStateNormal];
        [self.queryDictionary setObject:category.type_id forKey:@"type_id"];
    }
    else if ([keystring isEqualToString:@"brand"])
    {
        NSLog(@"brand_id -%@",[category.brand_id stringValue]);

        [self.brandButton setTitle:category.name forState:UIControlStateNormal];
        [self.queryDictionary setObject:category.brand_id forKey:@"brand_id"];
    }
    else if ([keystring isEqualToString:@"material"])
    {
        NSLog(@"material_id -%@",[category.material_id stringValue]);

        [self.modelButton setTitle:category.material_no forState:UIControlStateNormal];
        self.priceTextField.text = [category.price stringValue];
        [self.queryDictionary setObject:category.material_id forKey:@"material_id"];
        self.addCategory = category;
    }
}
- (void)loadView
{
    [super loadView];
    

    CGRect priceTextRect = self.priceTextField.frame;
    CGRect numberTextRect = self.numberTextField.frame;

    priceTextRect.size.height = 26;
    numberTextRect.size.height = 26;
    
    self.priceTextField.frame = priceTextRect;
    self.numberTextField.frame = numberTextRect;
    
    self.priceTextField.text = @"";
    self.numberTextField.text = @"1";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dictionary = [[NSMutableDictionary alloc]init];
    self.queryDictionary = [[NSMutableDictionary alloc]init];
    self.addCategory = nil;
    // Do any additional setup after loading the view from its nib.
    UIImage *imageBtn = [UIImage imageNamed:@"3355_05"];
    UIImage *backgroundButtonImage = [UIImage imageNamed:@"3355_05"];
    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
    
    UIButton *subitButton = [[UIButton alloc] initWithFrame:buttonRect];
    [subitButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    subitButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    subitButton.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
    [subitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subitButton setTitle:@"添加" forState:UIControlStateNormal];
    [subitButton addTarget:self action:@selector(addParts) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *subitItem = [[UIBarButtonItem alloc] initWithCustomView:subitButton];
    self.navigationItem.rightBarButtonItem = subitItem;
    

    [self queryPartsDate:@"category"];
    [self queryPartsDate:@"type"];
    [self queryPartsDate:@"brand"];
    [self queryPartsDate:@"material"];
    [self requestDictionary];
    self.priceTextField.delegate = self;
    self.numberTextField.delegate = self;
}
- (void)requestDictionary
{
    [self.dictionary setObject:REQ_DICTIONARY forKey:KEY_DREQTYPE];

    [self.rootDelegate.viewController generalRequest:self.dictionary tag:REQUESTED_GETDICTIONARY_CODE];
    
}
- (void)queryPartsDate:(NSString *)tableName
{
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modify_date"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag = %@",tableName];
    [request setPredicate:predicate];
    [request setFetchBatchSize:20];
    
    NSError *error = nil;
    //执行获取数据请求，返回数组
    NSMutableArray *mutableFetchResult = [[self.rootDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    if ([mutableFetchResult count] > 0) {
         Category *category= [mutableFetchResult objectAtIndex:0];
        NSLog(@"date-%@ == %@",category.modify_date,tableName);
        [self.dictionary setObject:category.modify_date forKey:tableName];
    }
    else
    {
        [self.dictionary setObject:@"1990-12-12 20:20:20" forKey:tableName];
    }
}
#pragma mark 添加配件
- (void)addParts
{
    if ([self.numberTextField.text intValue] < 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"配件数量不能为0" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    if (self.addCategory ==nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请选择配件" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    NSLog(@"self.addCategory=%@",self.addCategory.material_no);

    [delegate setPartCategory:self.addCategory amount:[self.numberTextField.text intValue]];
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark textfiled
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

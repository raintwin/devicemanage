//
//  maintaintraderViewCotroller.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "maintaintraderViewCotroller.h"
#import "MiantainTraderCell.h"


static NSString *const MiantainTraderIdentifier = @"MiantainTraderCellIdentifier";

@interface maintaintraderViewCotroller ()

@end

@implementation maintaintraderViewCotroller
@synthesize tableView = _tableView;
@synthesize fetchedResultsController = _fetchedResultsController;

@synthesize isDidSelect;
@synthesize Delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [[[self.fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MaintainTraderItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    MiantainTraderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MiantainTraderIdentifier];
    
    [cell populateCellWithEvent:item];
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MaintainTraderItem *maintainTraderItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;

    cell.textLabel.text = maintainTraderItem.name;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isDidSelect) {
        MaintainTraderItem *maintainTraderItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [Delegate maintainTraderInfo:maintainTraderItem];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.rootDelegate.viewController requestMaintainTrader];

    [self.tableView registerNib:[UINib nibWithNibName:@"MiantainTraderCell" bundle:nil] forCellReuseIdentifier:MiantainTraderIdentifier];
}


- (NSFetchedResultsController *)fetchedResultsController
{
    if (nil != _fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *playerEntity = [NSEntityDescription entityForName:@"MaintainTraderItem" inManagedObjectContext:self.rootDelegate.managedObjectContext];
    [fetchRequest setEntity:playerEntity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"repairId"ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.rootDelegate.managedObjectContext sectionNameKeyPath:nil cacheName:@"MaintainTraderItem"];
    _fetchedResultsController.delegate = self;
    
    NSError *error = NULL;
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _fetchedResultsController;
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller;
{
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

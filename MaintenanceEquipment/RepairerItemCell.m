//
//  RepairerItemCell.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-17.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "RepairerItemCell.h"

@implementation RepairerItemCell
@synthesize nameLabel;
@synthesize phoneLabel;
@synthesize markButton;
@synthesize isMark;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)awakeFromNib
{
    self.nameLabel.text = @"";
    self.phoneLabel.text = @"";
    self.markButton.hidden = YES;
}
- (void)populateCellWithEvent:(Repairer *)item;
{
    self.nameLabel.text = item.name;
    self.phoneLabel.text = item.phone;
    if (isMark) {
        self.markButton.hidden = NO;
    }
}
@end

//
//  MountingsViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

typedef enum _partsType {
    
    PARTS_CATEGORY = 0,
    PARTS_TYPE,
    PARTS_BRAND,
    PARTS_MATERIAL,
}PartsType;

#import "rootViewController.h"
@protocol MountingsViewControllerDelegate <NSObject>
-(void)setPartCategory:(Category *)category amount:(int)amount;
@end

@interface MountingsViewController : ViewController<UITextFieldDelegate>

@property (assign,nonatomic)id <MountingsViewControllerDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIButton *largeButton;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UIButton *modelButton;
@property (weak, nonatomic) IBOutlet UIButton *brandButton;

@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;



- (IBAction)largeButton:(id)sender;
- (IBAction)typeButton:(id)sender;
- (IBAction)modelButton:(id)sender;
- (IBAction)brandButton:(id)sender;


@end

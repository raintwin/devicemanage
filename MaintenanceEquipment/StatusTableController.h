//
//  StatusTableController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-23.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StatusTableControllerDelegate <NSObject>

-(void)setStatusIndex:(NSString *)index;

@end
@interface StatusTableController : UITableViewController

@property (nonatomic,assign)id <StatusTableControllerDelegate>delegate;
@end

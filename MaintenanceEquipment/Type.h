//
//  Type.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-19.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Type : NSManagedObject

@property (nonatomic, retain) NSString * type_id;
@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * modify_date;

@end

//
//  DeviceRapairViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-16.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "DeviceRapairViewController.h"

@interface DeviceRapairViewController ()

@end

@implementation DeviceRapairViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

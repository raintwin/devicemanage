//
//  rootViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-6-27.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "rootViewController.h"
#import "ASIFormDataRequest.h"

#import "MaintainTraderItem.h"

#define HEXCOLOR(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


NSString * const FSPDeviceViewController = @"DeviceViewController";
NSString * const FSPMaintainViewController = @"MaintainViewController";
NSString * const FSPLoanDeviceViewController = @"LoanDeviceViewController";
NSString * const FSPLoanItemViewController = @"LoanItemViewController";
NSString * const FSPMaintainTraderViewController = @"maintaintraderViewCotroller";
NSString * const FSPMaintainRecordViewController = @"MaintainRecordViewController";

NSString * const FSPDeviceRapairViewController = @"DeviceRapairViewController";


NSString * const FSPRepairerItemViewController = @"RepairerItemViewController";

@interface rootViewController ()
@property (nonatomic,retain) NSMutableArray *entries;
@property (nonatomic,retain) NSMutableArray *functionItem;
@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,strong) UINavigationController *navController;

@end

@implementation rootViewController
@synthesize loginButton = _loginButton;
@synthesize loginAgainButton = _loginAgainButton;
@synthesize tableView = _tableView;
@synthesize myDelegate;
@synthesize entries;
@synthesize functionItem;
@synthesize characterId;
@synthesize mainRecordInofViewController;

@synthesize useNameTextField;
@synthesize passWordTextField;
@synthesize urlTextField;

@synthesize navController;

typedef enum _viewType {
    
    MEDEVICEITEMVIEWTYPE = 0,
    MELOANDEVICEVIEWTYPE,
    MELOANLITEMVIEWTYPE,
    MEMAINTAINTRADERTYPE,
    MEMAINTAINRECORDTYPE,
    MEMAINTIANDEVICEVIEWTYPE,
    
    
    MEDEVICEREPAIRRECORDTYPE
    

}ViewType;

typedef enum _ApplyviewType {
    
    MEDEVICEITEMVIEWAPPLYTYPE = 0,
    MEMAINTAINRECORDAPPLYTYPE 
}ApplyviewType;

typedef enum _repairViewType {
    MEREPAIRERTYPE = 0,
    MEREPAIRDEVICEVIEWTYPE ,

}RepairViewType;

#pragma mark -- 
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource  = self;
    self.tableView.backgroundColor = HEXCOLOR(0x3f3f3f);
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setSeparatorColor:[UIColor blackColor]];
    [self.tableView setScrollEnabled:NO];
    [self.view addSubview:self.tableView];
    self.tableView.hidden = YES;
    
    self.loginAgainButton.hidden = YES;
    [self.loginAgainButton setFrame:CGRectMake(0, self.view.bounds.size.height-40, 282, 40)];
    [self.view bringSubviewToFront:self.loginAgainButton];
    self.myDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	// Do any additional setup after loading the view, typically from a nib.
    self.mainRecordInofViewController = nil;
    self.useNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:DAFUSENAME];
    self.passWordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:DAFPASSWORD];
    
    self.urlTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:DAFADDRESS];
    
    self.passWordTextField.secureTextEntry = YES;
    
    self.useNameTextField.delegate = self;
    self.passWordTextField.delegate = self;
    self.urlTextField.delegate = self;
    
    UIImage *imageBtn = [UIImage imageNamed:@"提交申请_03"];
    UIImage *backgroundButtonImage = imageBtn;
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
    
    UIButton *queryButton = [[UIButton alloc] initWithFrame:buttonRect];
    [queryButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [queryButton setTitle:@"重新登录" forState:UIControlStateNormal];
    queryButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [queryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [queryButton addTarget:self action:@selector(logBackIn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:queryButton];
    self.navigationItem.rightBarButtonItem = closeItem;
    
}
-(void)logBackIn
{
    self.tableView.hidden = YES;
//    self.navigationController.navigationBarHidden = YES;
}
- (void)query {
    //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DeviceItem"inManagedObjectContext:self.myDelegate.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"barcode"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    
    
    NSError *error = nil;
    //执行获取数据请求，返回数组
    NSMutableArray *mutableFetchResult = [[self.myDelegate.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    self.entries = mutableFetchResult;
    
    NSLog(@"The count of entry:%i",[self.entries count]);

}
- (void)saveData
{
    DeviceItem *DeviceItem = [NSEntityDescription insertNewObjectForEntityForName:@"DeviceItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
    
    DeviceItem.barcode = @"1";
    DeviceItem.deviceStatus = @"2";
    DeviceItem.buyDate = @"3";
    DeviceItem.type = @"3";
    DeviceItem.category = @"3";
    DeviceItem.deviceNo = @"3";
    DeviceItem.deviceName = @"3";
    DeviceItem.brand = @"3";
    DeviceItem.price = [NSNumber numberWithInt:3000];
    DeviceItem.deviceId = [NSNumber numberWithInt:11];
    
    NSError *error;
    
    //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
    BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
    
    if (!isSaveSuccess) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }else {
//        NSLog(@"Save equit successful!");
    }
}

#pragma mark 
#pragma mark 登录


- (IBAction)login:(id)sender
{
//    NSURL *url  = [NSURL URLWithString:@"health://cn.best1101.www.health1ddd"];
//    [[UIApplication sharedApplication] openURL:url];
    [self textFiledReturnEditing];
    if ([self.useNameTextField.text length] == 0  || [self.passWordTextField.text length] == 0 ) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"用户名或密码错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertview show];
        return;
    }
    if ([self.urlTextField.text length] == 0) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请确认请求地址" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertview show];
        return;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:self.useNameTextField.text forKey:@"userNo"];
    [dict setObject:self.passWordTextField.text forKey:@"parssword"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] forKey:@"mobileCode"];
    [dict setObject:REQ_LOGINCODE forKey:KEY_DREQTYPE];
    [dict setObject:MOBILETYPE forKey:@"mobileType"];
    [self generalRequest:dict tag:REQUESTED_LOGIN_CODE];
}

- (IBAction)loginAgain:(id)sender;
{
    [SVProgressHUD show];
    self.tableView.hidden = YES;
    self.loginAgainButton.hidden = YES;

    for (UIViewController *controller in self.navController.viewControllers) {

        [controller.view removeFromSuperview];
        controller.view = nil;
    }
    [self.navController.view removeFromSuperview];
    self.navController = nil;
    [SVProgressHUD dismiss];
}

#pragma mark

#pragma mark 设备列表
- (void) requestEquipItem;
{
    int schoolId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] intValue];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:REQ_EQUIPCODE forKey:KEY_DREQTYPE];
    [dict setObject:[NSNumber numberWithInt:schoolId] forKey:@"schoolId"];
    
    [dict setObject:@"" forKey:@"deviceName"];
    [dict setObject:@"" forKey:@"barcode"];
    [dict setObject:@"" forKey:@"deviceNo"];
    
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [dict setObject:[NSNumber numberWithInt:100] forKey:@"everyPage"];
    
    [self generalRequest:dict tag:REQUESTED_EQUIPITEM_CODE];
}
- (void) requestEquipItemOfDeviceNo:(NSString *)deviceNo;
{
    int schoolId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] intValue];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:REQ_EQUIPCODE forKey:KEY_DREQTYPE];
    [dict setObject:[NSNumber numberWithInt:schoolId] forKey:@"schoolId"];
    
    [dict setObject:@"" forKey:@"deviceName"];
    [dict setObject:@"" forKey:@"barcode"];
    [dict setObject:deviceNo forKey:@"deviceNo"];
    
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [dict setObject:[NSNumber numberWithInt:100] forKey:@"everyPage"];
    
    [self generalRequest:dict tag:REQUESTED_EQUIPITEM_CODE];
}
#pragma mark

#pragma mark 维修商
- (void) requestMaintainTrader;
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:REQ_MAINTAINTRADER forKey:KEY_DREQTYPE];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [dict setObject:[NSNumber numberWithInt:1] forKey:KEY_PAGE];
    [dict setObject:[NSNumber numberWithInt:100] forKey:KEY_EVERYPAGE];
    
    [self generalRequest:dict tag:REQUESTED_MAINTAINTRADER_CODE];
}
#pragma mark

#pragma mark 借出列表
- (void) requestLoanItem;
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:REQ_LOANITEM forKey:KEY_DREQTYPE];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:@"" forKey:@"deviceNo"];
    [dict setObject:@"" forKey:@"status"];
    [dict setObject:@"" forKey:@"lendDateF"];
    [dict setObject:@"" forKey:@"lendDateL"];

    [dict setObject:[NSNumber numberWithInt:1] forKey:KEY_PAGE];
    [dict setObject:[NSNumber numberWithInt:1000] forKey:KEY_EVERYPAGE];
    
    [self generalRequest:dict tag:REQUESTED_LOANITEM_CODE];
}
- (void) requestLoanItemOfDeviceNo:(NSString *)deviceNo;
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:REQ_LOANITEM forKey:KEY_DREQTYPE];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"schoolId"] forKey:@"schoolId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:deviceNo forKey:@"deviceNo"];
    [dict setObject:@"" forKey:@"status"];
    [dict setObject:@"" forKey:@"lendDateF"];
    [dict setObject:@"" forKey:@"lendDateL"];
    
    [dict setObject:[NSNumber numberWithInt:1] forKey:KEY_PAGE];
    [dict setObject:[NSNumber numberWithInt:100] forKey:KEY_EVERYPAGE];
    
    [self generalRequest:dict tag:REQUESTED_LOANITEM_CODE];
}
#pragma mark

#pragma mark 维修记录列表
- (void) requestMaintainRecord
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [dict setObject:[analysisJson getCharterReq] forKey:KEY_DREQTYPE];
    [dict setObject:@"" forKey:@"deviceName"];
    [dict setObject:@"" forKey:@"deviceStatus"];
    [dict setObject:@"" forKey:@"deviceNo"];

    [dict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [dict setObject:[NSNumber numberWithInt:100] forKey:@"everyPage"];

    [self generalRequest:dict tag:REQUESTED_MAINTAINRECORD_CODE];
}
- (void) requestMaintainRecordOfDeviceNo:(NSString *)deviceNO
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [dict setObject:[analysisJson getCharterReq] forKey:KEY_DREQTYPE];
    [dict setObject:@"" forKey:@"deviceName"];
    [dict setObject:@"" forKey:@"deviceStatus"];
    [dict setObject:deviceNO forKey:@"deviceNo"];
    
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [dict setObject:[NSNumber numberWithInt:100] forKey:@"everyPage"];
    
    [self generalRequest:dict tag:REQUESTED_MAINTAINRECORD_CODE];
}
- (void)requestMaintainRecordForRepairOfDeviceNo:(NSString *)deviceNo
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    [dict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [dict setObject:[analysisJson getCharterReq] forKey:KEY_DREQTYPE];
    [dict setObject:@"" forKey:@"deviceName"];
    [dict setObject:@"" forKey:@"deviceStatus"];
    [dict setObject:deviceNo forKey:@"deviceNo"];
    
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [dict setObject:[NSNumber numberWithInt:100] forKey:@"everyPage"];
    
    [self generalRequest:dict tag:REQUESTED_MAINTAINRECORD_CODE];
}
#pragma mark

#pragma mark 设备列表 维修商
- (void) requestMaintainRecordForRepair
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [dict setObject:REQ_REPAIRRECORD forKey:KEY_DREQTYPE];
    [dict setObject:@"" forKey:@"deviceName"];
    [dict setObject:@"" forKey:@"deviceStatus"];
    [dict setObject:@"" forKey:@"deviceNo"];
    
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [dict setObject:[NSNumber numberWithInt:100] forKey:@"everyPage"];
    [self generalRequest:dict tag:REQUESTED_MAINTAINRECORD_CODE];
}

#pragma mark

#pragma mark 维修员
- (void) requestRepairerItem
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[analysisJson getCharterVulue] forKey:[analysisJson getCharterKey]];
    [dict setObject:REQ_REPAIRER forKey:KEY_DREQTYPE];
    [self generalRequest:dict tag:REQUESTED_REPAIRER_CODE];
    
}
#pragma mark 请求
- (void)generalRequest:(NSMutableDictionary *)postValue tag:(RequestCode)flag;
{
    NSString *urlstrng = [self getFormatUrlstring:self.urlTextField.text];
    
    NSLog(@"urlstrng :%@",urlstrng);
    NSLog(@"requestCore = %@",[postValue JSONRepresentation]);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlstrng]];
    request.tag = flag;
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:[postValue JSONRepresentation] forKey:@"requestContent"];
    [request startAsynchronous];
    [SVProgressHUD show];

}

- (void)generalRequest:(NSMutableDictionary *)postValue tag:(RequestCode)flag delegate:(MainRecordInofViewController *)mainDelegate;
{
    NSString *urlstrng = [self getFormatUrlstring:self.urlTextField.text];

    NSLog(@"requestCore = %@",[postValue JSONRepresentation]);
    self.mainRecordInofViewController = mainDelegate;
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlstrng]];
    request.tag = flag;
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:[postValue JSONRepresentation] forKey:@"requestContent"];
    [request startAsynchronous];
    [SVProgressHUD show];


}
-(NSString *)getFormatUrlstring:(NSString *)urlStirng 
{
//    #define WERVERURL   @"http://www.best1101.net/MoblieAction_DMInterface"
    NSString *werverUrl = @"http://";
    if ([urlStirng length] > 0) {
        werverUrl = [werverUrl stringByAppendingString:urlStirng];
    }

    
    werverUrl = [werverUrl stringByAppendingString:@"/MoblieAction_DMInterface"];
    return werverUrl;
}
#pragma mark

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    NSLog(@"---- responseStr %@",responseStr);
    
    NSString *resultCode = [[responseStr JSONValue] objectForKey:KEY_DRESULTCODE];
    int requesttag = request.tag;
    
    NSString *msg  = [[responseStr JSONValue] objectForKey:KEY_DMSG];

    if ([resultCode isEqualToString:@"0"]) {
        switch (requesttag) {
            case REQUESTED_LOGIN_CODE: // 登录
                [analysisJson analysisLoginJson:[responseStr JSONValue]];
                [[NSUserDefaults standardUserDefaults] setObject:self.useNameTextField.text forKey:DAFUSENAME];
                [[NSUserDefaults standardUserDefaults] setObject:self.passWordTextField.text forKey:DAFPASSWORD];
                [[NSUserDefaults standardUserDefaults] setObject:self.urlTextField.text forKey:DAFADDRESS];

                [self initCharacterInof];
                self.tableView.hidden = NO;
                self.loginAgainButton.hidden = NO;
                if (characterId == CTSCHOOLTYPE) {
                    [self initViewControllerForSchool:MEDEVICEITEMVIEWTYPE];
                }
                else if (characterId == CTREAPAIRTYPE)
                {
                    [self initViewControllerForSchool:MEDEVICEREPAIRRECORDTYPE];
                }else if (characterId == CTSCHOOLAPPLYTYPE)
                {
                    [self initViewControllerForSchoolApply:MEDEVICEITEMVIEWAPPLYTYPE];

                }
                break;
            case REQUESTED_EQUIPITEM_CODE: // 设备列表
                [self analysisEquipItem:[[responseStr JSONValue] valueForKey:@"devices"]];
                
                break;
            case REQUESTED_APPLYREPAIR_DODE: // 提交申请维修成功
            {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"提交成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertview.delegate = self;
                alertview.tag = REQUESTED_APPLYREPAIR_DODE;
                [alertview show];
            }
                break;
            case REQUESTED_SUBMITLOAN_CODE: //提交借出
                {
                    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"提交成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    alertview.tag = REQUESTED_SUBMITLOAN_CODE;
                    [alertview show];

                }
                break;
            case REQUESTED_MAINTAINTRADER_CODE: // 维修商
                [self analysisMaintainTrader:[[responseStr JSONValue] valueForKey:@"list"]];
                break;
            case REQUESTED_LOANITEM_CODE: //借出列表
                [self analysisLoanItem:[[responseStr JSONValue] valueForKey:@"lendDevices"]];
                break;
                
            case REQUESTED_REVERTDEVICE_DODE: //归还
            {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"归还成功" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertview.tag = REQUESTED_REVERTDEVICE_DODE;
                alertview.delegate = self;
                [alertview show];
            }
            case REQUESTED_MAINTAINRECORD_CODE: //维修记录
                [self analysisgetDataInfoMaintainRecordItem:[[responseStr JSONValue] valueForKey:@"list"]];
                break;
            case REQUESTED_MAINTAINTCANCEL_CODE: //撤销申请
            {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"撤销申请完成" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertview.tag = REQUESTED_MAINTAINTCANCEL_CODE;
                alertview.delegate = self;
                [alertview show];
            }
                
                break;
            case REQUESTED_REPAIRER_CODE: // 维修员列表
                [self analysisRepairerItem:[[responseStr JSONValue] valueForKey:@"list"]];
                break;
                
            case REQUESTED_DELEGATEREPAIRER: // 委托 维修员
            {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"委派成功" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertview.tag = REQUESTED_DELEGATEREPAIRER;
                alertview.delegate = self;
                [alertview show];
                [self.navController popToRootViewControllerAnimated:YES];
            }

                break;
            case REQUESTED_GETREPAIRER:  // 获取已委托 维修员
                [self analysisDelegatedRepairer:[[responseStr JSONValue] valueForKey:@"list"]];
                break;
                
            case REQUESTED_GETDICTIONARY_CODE: //配件字典
                [self analysisDictionary:[[responseStr JSONValue] valueForKey:@"category"]name:@"category"];
                [self analysisDictionary:[[responseStr JSONValue] valueForKey:@"type"]name:@"type"];
                [self analysisDictionary:[[responseStr JSONValue] valueForKey:@"brand"]name:@"brand"];
                [self analysisDictionary:[[responseStr JSONValue] valueForKey:@"material"]name:@"material"];
                break;
            case REQUESTED_CORRESPODPARTS_CODE: // 对应的配件列表
                [self analysisCorrespondParts:[[responseStr JSONValue] valueForKey:@"list"]];
                break;
            case REQUESTED_SUBMITREPAIRINFO_CODE: // 提交维修信息
            {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提交成功" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertview.tag = REQUESTED_SUBMITREPAIRINFO_CODE;
                alertview.delegate = self;
                [alertview show];
            }

                
                break;
            case REQUESTED_RECEVICE_CODE: // 签收成功
            {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"签收成功" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                alertview.tag = REQUESTED_RECEVICE_CODE;
                alertview.delegate = self;
                [alertview show];
            }

                break;
            default:
                break;
        }
    }
    else
    {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertview show];
    }
    [SVProgressHUD dismiss];
}
- (void)requestFailed:(ASIHTTPRequest *)request{
    [SVProgressHUD dismiss];

    switch (request.tag) {
        case REQUESTED_SUBMITLOAN_CODE:
        case REQUESTED_APPLYREPAIR_DODE:
        case REQUESTED_REVERTDEVICE_DODE:
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:DEFDEVICENO];
            break;
            
        default:
            break;
    }
    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"网络不给力" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertview show];
}
- (void)initCharacterInof
{
    characterId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"type"] integerValue];
    switch (characterId) {
        case CTSCHOOLTYPE:
            self.functionItem = [[NSMutableArray alloc]initWithObjects:@"设备管理",@"借出登记",@"借出记录",@"维修商",@"维修记录" ,nil];
            break;
        case CTREAPAIRTYPE:
            self.functionItem = [[NSMutableArray alloc]initWithObjects:@"维修员",@"维修记录",nil];
            break;
        case CTSCHOOLAPPLYTYPE:
            self.functionItem = [[NSMutableArray alloc]initWithObjects:@"设备管理",@"维修记录",nil];
            break;
        default:
            break;
    }
    [self.tableView reloadData];
}
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    switch (alertView.tag) {
        case REQUESTED_REVERTDEVICE_DODE: //----- 刷新 借出列表
        [self requestLoanItemOfDeviceNo:[[NSUserDefaults standardUserDefaults] objectForKey:DEFDEVICENO]];
            break;
        case REQUESTED_RECEVICE_CODE: //----- 签收成功 刷新 维修记录
            [self successReceiveRepair];
            [self requestMaintainRecordOfDeviceNo:[[NSUserDefaults standardUserDefaults] objectForKey:DEFDEVICENO]];

            break;
        case REQUESTED_DELEGATEREPAIRER: // -------- 委派
        case REQUESTED_SUBMITREPAIRINFO_CODE: // --------- 提交维修情况
            [self requestMaintainRecordForRepairOfDeviceNo:[[NSUserDefaults standardUserDefaults] objectForKey:DEFDEVICENO]];
            [self.navController popViewControllerAnimated:YES];

            break;
        case REQUESTED_MAINTAINTCANCEL_CODE:  //----- 刷新 维修记录
            [self requestMaintainRecordOfDeviceNo:[[NSUserDefaults standardUserDefaults] objectForKey:DEFDEVICENO]];
            break;
        case REQUESTED_APPLYREPAIR_DODE: // ----- 刷新 维修申请
        case REQUESTED_SUBMITLOAN_CODE:  // ----- 设备列表
            [self requestEquipItemOfDeviceNo:[[NSUserDefaults standardUserDefaults] objectForKey:DEFDEVICENO]];
            break;
        default:
            break;
    }

}
- (void)successReceiveRepair
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[MaintainRecordViewController class]]) {
            [controller.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
}
#pragma mark -
-(void)initViewControllerForSchool:(ViewType)viewType
{
    NSString *viewNibName = nil;
    NSString *viewTitle = nil;
        switch (viewType) {
            case MEDEVICEITEMVIEWTYPE:
                viewNibName = FSPDeviceViewController;
                viewTitle = @"设备管理";
                break;
            case MELOANDEVICEVIEWTYPE:
                viewNibName = FSPLoanDeviceViewController;
                viewTitle = @"设备借出登记";
                break;
            case MELOANLITEMVIEWTYPE:
                viewNibName = FSPLoanItemViewController;
                viewTitle = @"借出记录";
                break;
            case MEMAINTAINTRADERTYPE: //维修商
                viewNibName = FSPMaintainTraderViewController;
                viewTitle = @"维修商";
                break;
            case MEMAINTAINRECORDTYPE:
                viewNibName = FSPMaintainRecordViewController;
                viewTitle = @"维修记录";
                break;
            case MEDEVICEREPAIRRECORDTYPE:
                viewNibName = FSPMaintainRecordViewController;
                
                viewTitle = @"设备维修记录";

                break;
            default:
                break;
        }

    [self loadViewController:viewNibName viewTitle:viewTitle];
}
-(void)initViewControllerForSchoolApply:(ApplyviewType)viewType
{
    NSString *viewNibName = nil;
    NSString *viewTitle = nil;
    switch (viewType) {
        case MEDEVICEITEMVIEWAPPLYTYPE:
            viewNibName = FSPDeviceViewController;
            viewTitle = @"设备管理";
            break;;
        case MEMAINTAINRECORDAPPLYTYPE:
            viewNibName = FSPMaintainRecordViewController;
            viewTitle = @"维修记录";
            break;
        default:
            break;
    }

    [self loadViewController:viewNibName viewTitle:viewTitle];
}

-(void)initViewControllerForRapir:(RepairViewType)viewType
{
    NSString *viewNibName = nil;
    NSString *viewTitle = nil;
    switch (viewType) {
        case MEREPAIRERTYPE:
            viewNibName = FSPRepairerItemViewController;
            viewTitle = @"维修员";
            break;
        case MEREPAIRDEVICEVIEWTYPE:
            viewNibName = FSPMaintainRecordViewController;
            viewTitle = @"设备维修记录";
            
            break;
        default:
            break;
    }
    
    [self loadViewController:viewNibName viewTitle:viewTitle];

    
}
- (void)loadViewController:(NSString *)viewNibName viewTitle:(NSString *)viewTitle
{
    Class viewcontrollerClass = NSClassFromString(viewNibName);
    
    for (UIViewController *controller in self.navController.viewControllers) {
        if ([controller isKindOfClass:viewcontrollerClass]) {
            [UIView animateWithDuration:0.3 animations:^{
                [self.navController.view setFrame:self.view.bounds];
            }];
            return;
        }
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.navController.view setFrame:self.view.bounds];
    }
                     completion:^(BOOL finish)
     {
         [self.navController.view removeFromSuperview];
         self.navController = nil;
         UIViewController *viewcontroller = [[viewcontrollerClass alloc]initWithNibName:viewNibName bundle:nil];
         viewcontroller.title = viewTitle;
         self.navController = [[UINavigationController alloc]initWithRootViewController:viewcontroller];
         [self.navController.view setFrame:self.view.bounds];
         
         [self.view addSubview:self.navController.view];
     }];

}
#pragma mark --

- (void)initFSPLoanItemViewController;
{
    Class viewcontrollerClass = NSClassFromString(FSPMaintainTraderViewController);
    UIViewController *viewcontroller = [[viewcontrollerClass alloc]initWithNibName:FSPMaintainTraderViewController bundle:nil];
    viewcontroller.title = @"维修商";
    [self.navigationController pushViewController:viewcontroller animated:YES];

}

#pragma mark 获取 设备列表
- (void)analysisEquipItem:(NSArray *)items;
{
    for (int i = 0 ; i < [items count]; i++) {
        
        DeviceItem *deviceItems = [self getDataInfoDeviceItem:[[items objectAtIndex:i] valueForKey:@"deviceId"]];
        
        if (nil == deviceItems) {
            deviceItems = [NSEntityDescription insertNewObjectForEntityForName:@"DeviceItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
        }
        deviceItems.barcode = [[items objectAtIndex:i] valueForKey:@"barcode"];
        deviceItems.deviceStatus = [[items objectAtIndex:i] valueForKey:@"deviceStatus"];
        deviceItems.buyDate = [[items objectAtIndex:i] valueForKey:@"buyDate"];
        deviceItems.type = [[items objectAtIndex:i] valueForKey:@"type"];
        deviceItems.category =[[items objectAtIndex:i] valueForKey:@"category"];
        deviceItems.deviceNo = [[items objectAtIndex:i] valueForKey:@"deviceNo"];
        deviceItems.deviceName = [[items objectAtIndex:i] valueForKey:@"deviceName"];
        deviceItems.brand = [[items objectAtIndex:i] valueForKey:@"brand"];
        deviceItems.price = [NSNumber numberWithInt:[[[items objectAtIndex:i] valueForKey:@"price"] intValue]];
        deviceItems.deviceId = [NSNumber numberWithInt:[[[items objectAtIndex:i] valueForKey:@"deviceId"] intValue]];
        NSError *error;
        
        //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
        BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
        
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
//            NSLog(@"Save equit successful!");
        }
    }
}
- (DeviceItem *)getDataInfoDeviceItem:(NSString *)keyId
{
    DeviceItem *teamObject = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *teamEntity = [NSEntityDescription entityForName:@"DeviceItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
    [fetchRequest setEntity:teamEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceId == %@", keyId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchLimit:1];
    
    NSError *error = NULL;
    NSArray *array = [self.myDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error : %@\n", [error localizedDescription]);
    }
    
    if (array && [array count] > 0) {
        teamObject = [array objectAtIndex:0];
    }
    
    fetchRequest = nil;
    
    return teamObject;
}

#pragma mark 获取 设备列表
- (void)analysisMaintainTrader:(NSArray *)items;
{
    for (int i = 0 ; i < [items count]; i++) {
        
        MaintainTraderItem *maintainTraderItem = [self getDataInfoMaintainTraderItem:[[items objectAtIndex:i] valueForKey:@"repairId"]];
        
        if (nil == maintainTraderItem) {
            maintainTraderItem = [NSEntityDescription insertNewObjectForEntityForName:@"MaintainTraderItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
        }
        maintainTraderItem.name = [[items objectAtIndex:i] valueForKey:@"name"];
        maintainTraderItem.telephone = [[items objectAtIndex:i] valueForKey:@"telephone"];
        maintainTraderItem.contact = [[items objectAtIndex:i] valueForKey:@"contact"];
        maintainTraderItem.repairId = [NSNumber numberWithInt:[[[items objectAtIndex:i] valueForKey:@"repairId"] intValue]];
        NSError *error;
        
        //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
        BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
        
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
//            NSLog(@"Save equit successful!");
        }
    }
}
- (MaintainTraderItem *)getDataInfoMaintainTraderItem:(NSString *)keyId
{
    MaintainTraderItem *teamObject = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *teamEntity = [NSEntityDescription entityForName:@"MaintainTraderItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
    [fetchRequest setEntity:teamEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"repairId == %@", keyId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchLimit:1];
    
    NSError *error = NULL;
    NSArray *array = [self.myDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error : %@\n", [error localizedDescription]);
    }
    
    if (array && [array count] > 0) {
        teamObject = [array objectAtIndex:0];
    }
    
    fetchRequest = nil;
    
    return teamObject;
}

#pragma mark 获取 借出列表
- (void)analysisLoanItem:(NSArray *)items;
{
    [self getDataInfoLoanItem];
    for (int i = 0 ; i < [items count]; i++) {
        
        LoanDeviceItem *loanDeviceItem = [NSEntityDescription insertNewObjectForEntityForName:@"LoanDeviceItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
        

        
        loanDeviceItem.deviceName = [[items objectAtIndex:i] valueForKey:@"deviceName"];
        loanDeviceItem.deviceNo = [[items objectAtIndex:i] valueForKey:@"deviceNo"];
        loanDeviceItem.lendUser = [[items objectAtIndex:i] valueForKey:@"lendUser"];
        loanDeviceItem.deptName = [[items objectAtIndex:i] valueForKey:@"deptName"];
        loanDeviceItem.lendDate = [[items objectAtIndex:i] valueForKey:@"lendDate"];
        loanDeviceItem.returnDate =  [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"returnDate"]];
       

        loanDeviceItem.deviceStatus = [[items objectAtIndex:i] valueForKey:@"deviceStatus"];

        loanDeviceItem.landOutId = [NSNumber numberWithInt:[[[items objectAtIndex:i] valueForKey:@"landOutId"] intValue]];
        NSError *error;
        
        //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
        BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
        
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
//            NSLog(@"Save equit successful!");
        }
    }
}
- (void )getDataInfoLoanItem
{
     ////
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LoanDeviceItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];

    NSError *error;
    NSArray *items = [self.myDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];


    for (LoanDeviceItem *managedObject in items) {
        [self.myDelegate.managedObjectContext deleteObject:managedObject];
    }
    if (![self.myDelegate.managedObjectContext save:&error]) {
        NSLog(@"Error:%@,%@",error,[error userInfo]);
    }


}
#pragma mark 获取 维修记录
- (void)analysisgetDataInfoMaintainRecordItem:(NSArray *)items;
{
    [self getDataInfoMaintainRecordItem];
    for (int i = 0 ; i < [items count]; i++) {
        
        
     MaintainRecordItem *loanDeviceItem = [NSEntityDescription insertNewObjectForEntityForName:@"MaintainRecordItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
        loanDeviceItem.maintenanceId = [NSNumber numberWithInt:[[[items objectAtIndex:i] valueForKey:@"maintenanceId"] intValue]];

        loanDeviceItem.deviceName = [[items objectAtIndex:i] valueForKey:@"deviceName"];
        loanDeviceItem.deviceNo = [[items objectAtIndex:i] valueForKey:@"deviceNo"];
        loanDeviceItem.deviceType = [[items objectAtIndex:i] valueForKey:@"deviceType"];
        loanDeviceItem.userName = [[items objectAtIndex:i] valueForKey:@"userName"];
        
        
        loanDeviceItem.phone = [[items objectAtIndex:i] valueForKey:@"phone"];
        loanDeviceItem.address = [[items objectAtIndex:i] valueForKey:@"address"];
        loanDeviceItem.content = [[items objectAtIndex:i] valueForKey:@"content"];
        loanDeviceItem.repair = [[items objectAtIndex:i] valueForKey:@"repair"];
        loanDeviceItem.status = [NSNumber numberWithInt:[[[items objectAtIndex:i] valueForKey:@"status"] intValue]];

        loanDeviceItem.time = [[items objectAtIndex:i] valueForKey:@"time"];

        loanDeviceItem.completionTime = [[items objectAtIndex:i] valueForKey:@"completionTime"];
        loanDeviceItem.repairContent = [[items objectAtIndex:i] valueForKey:@"repairContent"];
        loanDeviceItem.otherMoney = [[[items objectAtIndex:i] valueForKey:@"otherMoney"] stringValue];
        loanDeviceItem.modifyDate =  [[items objectAtIndex:i] valueForKey:@"modifyDate"];
        loanDeviceItem.acceptFlag =  [[items objectAtIndex:i] valueForKey:@"acceptFlag"];
        loanDeviceItem.manflag =  [[items objectAtIndex:i] valueForKey:@"manflag"];
        loanDeviceItem.appraisal =  [[items objectAtIndex:i] valueForKey:@"appraisal"];

        NSError *error;
        
        //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
        BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
        
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
//            NSLog(@"Save equit successful!");
        }
    }
}
- (void)getDataInfoMaintainRecordItem;
{
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MaintainRecordItem" inManagedObjectContext:self.myDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [self.myDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (MaintainRecordItem *managedObject in items) {
        [self.myDelegate.managedObjectContext deleteObject:managedObject];
    }
    if (![self.myDelegate.managedObjectContext save:&error]) {
        NSLog(@"Error:%@,%@",error,[error userInfo]);
    }

}
#pragma mark 获取 维修人员列表
- (void)analysisRepairerItem:(NSArray *)items;
{
    for (int i = 0 ; i < [items count]; i++) {
        
        Repairer *repiarerItem = [self getDataInfoRepairerItem:[[items objectAtIndex:i] valueForKey:@"userId"]];
        
        if (nil == repiarerItem) {
            repiarerItem = [NSEntityDescription insertNewObjectForEntityForName:@"Repairer" inManagedObjectContext:self.myDelegate.managedObjectContext];
        }
        
        repiarerItem.userId = [[items objectAtIndex:i] valueForKey:@"userId"];
        repiarerItem.name = [[items objectAtIndex:i] valueForKey:@"name"];
        repiarerItem.phone = [[items objectAtIndex:i] valueForKey:@"phone"];

         
        NSError *error;
        
        //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
        BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
        
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
//            NSLog(@"Save equit successful!");
        }
    }
}

- (Repairer *)getDataInfoRepairerItem:(NSString *)keyId
{
    Repairer *teamObject = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *teamEntity = [NSEntityDescription entityForName:@"Repairer" inManagedObjectContext:self.myDelegate.managedObjectContext];
    [fetchRequest setEntity:teamEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %@", keyId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchLimit:1];
    
    NSError *error = NULL;
    NSArray *array = [self.myDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error : %@\n", [error localizedDescription]);
    }
    
    if (array && [array count] > 0) {
        teamObject = [array objectAtIndex:0];
    }
    
    fetchRequest = nil;
    
    return teamObject;
}
#pragma mark 获取 配件字典
- (void)analysisDictionary:(NSArray *)items name:(NSString *)itemName;
{
    for (int i = 0 ; i < [items count]; i++) {
        
        Category *category = [self getDataInfoDictionary:[[items objectAtIndex:i] valueForKey:@"modify_date"]];
        
        if (nil == category) {
            category = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:self.myDelegate.managedObjectContext];
        }
                
        category.category_id = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"category_id"]];
        category.name = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"name"]];
        category.modify_date = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"modify_date"]];
        
        category.material_no = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"material_no"]];
        category.brand_id = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"brand_id"]];
        category.type_id = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"type_id"]];
        category.material_id = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"material_id"]];
        category.price = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"price"]];
        category.modify_date = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"modify_date"]];
        category.flag = itemName;
        NSError *error;
        
        //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
        BOOL isSaveSuccess = [self.myDelegate.managedObjectContext save:&error];
        
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
//            NSLog(@"Save equit successful!");
        }
    }
}

- (Category *)getDataInfoDictionary:(NSString *)keyId
{
    Category *teamObject = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *teamEntity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:self.myDelegate.managedObjectContext];
    [fetchRequest setEntity:teamEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"modify_date == %@", keyId];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchLimit:1];
    
    NSError *error = NULL;
    NSArray *array = [self.myDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error : %@\n", [error localizedDescription]);
    }
    
    if (array && [array count] > 0) {
        teamObject = [array objectAtIndex:0];
    }
    
    fetchRequest = nil;
    
    return teamObject;
}
#pragma mark 获取 委派员列表
- (void)analysisDelegatedRepairer:(NSArray *)items
{
    if ([items count] > 0) {
        NSString *name = [analysisJson analysisJsonData:[[items objectAtIndex:0] valueForKey:@"name"]];
        NSString *phone = [analysisJson analysisJsonData:[[items objectAtIndex:0] valueForKey:@"phone"]];
        if (self.mainRecordInofViewController != nil) {
            self.mainRecordInofViewController.repairPersonLabel.text = [NSString stringWithFormat:@"%@ %@",name,phone];
            [self.mainRecordInofViewController getPartsItem];
        }
    }
    
}
#pragma mark 获取 对应的配件列表
- (void)analysisCorrespondParts:(NSArray *)items
{
    
    if (self.mainRecordInofViewController != nil) {
        NSMutableArray *correspondItems = [[NSMutableArray alloc]init];
        for (int i = 0 ; i < [items count]; i++) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            
            NSString *name = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"name"]];
            NSNumber *materialId = [analysisJson analysisJsonData:[[items objectAtIndex:i] valueForKey:@"materialId"]];
            NSNumber *price  = [[items objectAtIndex:i] valueForKey:@"price"];
            NSNumber *amount = [[items objectAtIndex:i] valueForKey:@"amount"];
            
            if (name == nil) {
                [dict setObject:@"" forKey:@"name"];
            }else{
                [dict setObject:name forKey:@"name"];}
            
            if (materialId != nil) {
                [dict setObject:materialId forKey:@"materialId"];
            }
            if (price != nil) {
                [dict setObject:price forKey:@"price"];
            }
            if (amount != nil) {
                [dict setObject:amount forKey:@"amount"];
            }
            [correspondItems addObject:dict];

        }
        [self.mainRecordInofViewController setPartsItem:correspondItems];   
    }
//    self.mainRecordInofViewController = nil;

}

#pragma mark -
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42.0f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"self.functionItem cout %d",[self.functionItem count]);
    return [self.functionItem count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TeamTableViewCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
//    cell.textLabel.text = [self.functionItem objectAtIndex:[indexPath row]];
    cell.imageView.image = [UIImage imageNamed:[self.functionItem objectAtIndex:[indexPath row]]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSLog(@"[indexPath row] ;%d",[indexPath row]);
    switch ([analysisJson getCharterType]) {
        case CTSCHOOLTYPE:
            [self initViewControllerForSchool:[indexPath row]];
            break;
        case CTREAPAIRTYPE:
            [self initViewControllerForRapir:[indexPath row]];
            break;
        case CTSCHOOLAPPLYTYPE:
            [self initViewControllerForSchoolApply:[indexPath row]];
            break;
        default:
            break;
    }
}
#pragma mark 
#pragma mark-----
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (void)textFiledReturnEditing
{
    [self.useNameTextField resignFirstResponder];
    [self.passWordTextField resignFirstResponder];
    [self.urlTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  RepairerItemCell.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-17.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repairer.h"
@interface RepairerItemCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *markButton;
@property (nonatomic,assign) BOOL isMark;
- (void)populateCellWithEvent:(Repairer *)item;

@end

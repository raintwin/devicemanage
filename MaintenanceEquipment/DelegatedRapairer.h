//
//  DelegatedRapairer.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-23.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DelegatedRapairer : NSManagedObject

@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * flag;

@end

//
//  MainRecordInofViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-15.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "MaintainRecordItem.h"
#import "MaintainRecordCell.h"
#import "rootViewController.h"
#import "DelegatedRapairer.h"
#import "StatusTableController.h"
@interface MainRecordInofViewController : ViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate>



@property (weak, nonatomic) IBOutlet UILabel *maintenanceIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *repairPersonLabel;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;
@property (weak, nonatomic) IBOutlet UILabel *comTimeLabel;

@property (weak, nonatomic) IBOutlet UILabel *deviceTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *repairLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UITextView *repairContentTextView;
@property (weak, nonatomic) IBOutlet UITextField *otherTextField;

@property (weak, nonatomic) IBOutlet UIButton *addAccessories;
@property (weak, nonatomic) IBOutlet UIButton *optionButton;
@property (weak, nonatomic) IBOutlet UIButton *receiveButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scorllView;
@property (nonatomic ,strong) NSFetchedResultsController *fetchedResultsController;


@property (nonatomic ,retain) MaintainRecordItem *maintainRecordItem;


- (void)populateCellWithEvent:(MaintainRecordItem *)item;
-(void)setDeviceStatus:(NSNumber *)deviceId;
-(void)getRepairer;
-(void) getPartsItem;
-(void)setPartCategory:(Category *)category amount:(int)amount;;
-(void)setPartsItem:(NSMutableArray *)item;
@end

//
//  ReceiveViewController.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-24.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaintainRecordItem.h"
#import "rootAppDelegate.h"
#import "ViewController.h"
#import "rootViewController.h"
#import "MaintainRecordViewController.h"
@class MaintainRecordViewController;
@interface ReceiveViewController : UITableViewController<UITextFieldDelegate>
{
    int accflag;
    int manflag;
    
    int accflagCurrentIndex;
    int manflagCurrentIndex;

}
-(void)successReceiveRepair;
@property (nonatomic ,retain) MaintainRecordItem *maintainRecordItem;
@property (strong,nonatomic) rootAppDelegate *myDelegate;

@end

//
//  MaintainTraderItem.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-11.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "MaintainTraderItem.h"


@implementation MaintainTraderItem

@dynamic repairId;
@dynamic name;
@dynamic telephone;
@dynamic contact;

@end

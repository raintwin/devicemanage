//
//  EquipView.h
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-4.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquipView : UIView

@property (weak, nonatomic) IBOutlet UILabel *equipNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *equipIdLabel;

@property (weak, nonatomic) IBOutlet UIButton * loanButton;
@property (weak, nonatomic) IBOutlet UIButton * maintainButton;
@property (weak, nonatomic) IBOutlet UIButton * maintainingButton;
@property (weak, nonatomic) IBOutlet UIButton * revertButton;
@property (weak, nonatomic) IBOutlet UIButton * badButton;
@property (weak, nonatomic) IBOutlet UIButton * popupCellButton;

@property (weak, nonatomic) IBOutlet UIButton * didReverButton;

//@property (strong ,nonatomic) IBOutlet UITextField *equipNameField;
//@property (strong ,nonatomic) IBOutlet UITextField *equipIdField;

@end

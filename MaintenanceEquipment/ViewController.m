//
//  ViewController.m
//  MaintenanceEquipment
//
//  Created by dinghao on 13-7-3.
//  Copyright (c) 2013年 dinghao. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()
@property (nonatomic,strong) UIView  *tipView;
@property (nonatomic,strong) UILabel *tipTextLabel;
@end

@implementation ViewController
@synthesize rootDelegate;
@synthesize tipView;
@synthesize tipTextLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.rootDelegate = (rootAppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = nil;
    
    UIImage *imageBtn = [UIImage imageNamed:@"功能菜单_05"];
    UIImage *backgroundButtonImage = imageBtn;
//    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
//                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
    [closeButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [closeButton setTitle:nil forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dropDownMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    self.navigationItem.leftBarButtonItem = closeItem;
    
    isDropMenu = NO;
    
    

}
#pragma mark 返回
-(void)setNavgationBack
{
    UIImage *imageBtn = [UIImage imageNamed:@"返回"];
    UIImage *backgroundButtonImage = imageBtn;
    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
    [closeButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [closeButton setTitle:nil forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    closeButton.titleEdgeInsets = UIEdgeInsetsMake(4, 0, 0, 0);
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dropDownMenuBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    self.navigationItem.leftBarButtonItem = closeItem;
}
#pragma mark 查询 扫描
- (void) setDeviceItemRightBarButtonItems
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    {
        UIImage *imageBtn = [UIImage imageNamed:@"学校设备信息_08"];
        UIImage *backgroundButtonImage = imageBtn;
        
        CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
        [closeButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
        [closeButton setTitle:@"查询" forState:UIControlStateNormal];
        closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(queryData) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
        
        [array addObject:closeItem];

    }
    {
        UIImage *imageBtn = [UIImage imageNamed:@"3355_05"];
        UIImage *backgroundButtonImage = imageBtn;
        
        CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
        [closeButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
        [closeButton setTitle:@"扫描" forState:UIControlStateNormal];
        closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(scanningDevice) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
        
        [array addObject:closeItem];
        
    }
    self.navigationItem.rightBarButtonItems = array;

}

-(void) setDeviceItemRightBarOfquery;
{
    UIImage *imageBtn = [UIImage imageNamed:@"学校设备信息_08"];
    UIImage *backgroundButtonImage = imageBtn;
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, imageBtn.size.height);
    
    UIButton *queryButton = [[UIButton alloc] initWithFrame:buttonRect];
    [queryButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [queryButton setTitle:@"查询" forState:UIControlStateNormal];
    queryButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [queryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [queryButton addTarget:self action:@selector(queryData) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:queryButton];
    self.navigationItem.rightBarButtonItem = closeItem;
}
#pragma mark 菜单
-(void)dropDownMenu
{
    CGRect viewControllerRect = [UIApplication sharedApplication].keyWindow.rootViewController.view.bounds;

    if (self.navigationController.view.frame.origin.x == 0.0) {
        viewControllerRect.origin.x = 282.0f;
        isDropMenu = YES;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.navigationController.view setFrame:viewControllerRect];
    }];
}
#pragma mark 返回
-(void)dropDownMenuBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  更新view 
-(void)setTipViewForTabelView:(UITableView *)tabelView;
{
    self.tipView = [[UIView alloc]initWithFrame:CGRectMake(0, -TipViewHeight, TipViewWidth, TipViewHeight)];
    self.tipTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, TipViewWidth, TipViewHeight)];
    
    self.tipTextLabel.font = [UIFont systemFontOfSize:15];
    self.tipTextLabel.textAlignment = UITextAlignmentCenter;
    self.tipTextLabel.backgroundColor = [UIColor clearColor];
    self.tipTextLabel.textColor = [UIColor blackColor];
    
    [tabelView addSubview:self.tipView];
    [self.tipView addSubview:self.tipTextLabel];

}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if (scrollView.contentOffset.y > - TipViewHeight ) {
        self.tipTextLabel.text = TipTextRelease;
    }
    else {
        self.tipTextLabel.text = TipTextPull;
    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate && scrollView.contentOffset.y < - TipViewHeight) {
        [self startLoading];
        return;
    }
}
- (void)startLoading {
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
